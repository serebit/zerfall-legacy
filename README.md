# Zerfall 

[![Discuss](https://img.shields.io/badge/discuss-Discord-blue.svg)](https://discord.gg/djJJzUQ) [![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=5TC7HWQ2AGGQW&lc=US&item_name=Exoplanet%20Software&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donate_LG%2egif%3aNonHosted) [![License](https://img.shields.io/badge/License-Apache%20v2-blue.svg)](https://www.apache.org/licenses/LICENSE-2.0)

Zerfall is a two-dimensional platformer built in Java where the only objective is to survive the endless waves of zombies for as long as possible.
