# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## [UNRELEASED]
### Fixed
- Odd bug where the skybox replaced itself with the previous game screen upon starting a
 new game for the second time.

## [v2.1.2-alpha](https://github.com/statictype/zerfall/releases/tag/v2.1.2-alpha) - 2017-01-03
### Fixed
- Fixed the issue with the executable file not opening on all systems.

## [v2.1.0-alpha](https://github.com/statictype/zerfall/releases/tag/v2.1.0-alpha) - 2017-01-02
### Added
- More zombie spawners.
- The MAC-11 machine pistol.
- The ability to buy ammo from wallbuys for half price.

### Changed
- Executable files will now be much smaller.
- Swapped M9A1 wallbuy for MAC-11 wallbuy.
- Moved pistol wallbuys to top floor.
- Melee now based on raycast rather than bounding box intersection.
- Slightly faster heal rate for player.
- The player now spawns on the top platform instead of in the air above it.
- Several weapon sounds updated.

### Fixed
- Most multithreading errors.
- Pausing the game now pauses all sounds.
- Bug where mystery box would alternate between semi-automatic and fully automatic weapons.
- Miscellaneous performance improvements.


## [v2.0.0-alpha](https://github.com/statictype/zerfall/releases/tag/v2.0.0-alpha) - 2016-12-25
### Added
- More window sizes.

### Changed
- Converted most files to Kotlin.
- Optimized Mystery Box.
- Updated and optimized sound system.

### Fixed
- Eliminated errors caused by multithreading.


## [v1.1.0-alpha](https://github.com/statictype/zerfall/releases/tag/v1.1.0-alpha) - 2016-11-04
### Added
- In-game pause menu, activated by pressing Escape.
- In-game options menu.
- Control scheme option for both options menus.
- An additional control scheme.

### Changed
- Added panning to the run and death zombie sounds.
- Mapped the melee action to the V button in default controls.
