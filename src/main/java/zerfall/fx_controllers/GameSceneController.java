package zerfall.fx_controllers;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DialogPane;
import javafx.scene.layout.AnchorPane;
import sttc.iso.input.ControlScheme;
import sttc.iso.input.Controller;
import zerfall.Main;
import zerfall.controls.ClassicControlScheme;
import zerfall.controls.DefaultControlScheme;

import java.net.URL;
import java.util.ResourceBundle;

/**
 *
 */
public class GameSceneController implements Initializable {
  @FXML
  private AnchorPane pauseMenu;
  @FXML
  private Button resumeButton;
  @FXML
  private Button optionsButton;
  @FXML
  private Button quitButton;

  @FXML
  private DialogPane optionPanel;
  @FXML
  private ChoiceBox controlSchemeOption;


  @SuppressWarnings("unchecked")
  @Override
  public void initialize(URL location, ResourceBundle resources) {
    pauseMenu.setVisible(false);
    resumeButton.setOnAction(evt -> Main.togglePaused());
    optionsButton.setOnAction(evt -> optionPanel.setVisible(true));
    quitButton.setOnAction(evt -> Main.openMainMenu());

    optionPanel.setVisible(false);

    controlSchemeOption.setItems(FXCollections.observableArrayList("DEFAULT", "CLASSIC"));
    controlSchemeOption.setValue(Controller.getSchemeName());

    Button optionApplyButton = (Button) optionPanel.lookupButton(ButtonType.APPLY);
    optionApplyButton.setOnAction(evt -> applyOptions());
    optionApplyButton.setText("APPLY");

    Button optionCloseButton = (Button) optionPanel.lookupButton(ButtonType.CLOSE);
    optionCloseButton.setOnAction(evt -> optionPanel.setVisible(false));
    optionCloseButton.setText("CLOSE");
  }

  private void applyOptions() {
    ControlScheme scheme = null;
    if (controlSchemeOption.getValue().equals("DEFAULT")) {
      scheme = new DefaultControlScheme();
    } else if (controlSchemeOption.getValue().equals("CLASSIC")) {
      scheme = new ClassicControlScheme();
    }
    Controller.setControlScheme(scheme);
  }
}
