package zerfall.fx_controllers;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.SceneAntialiasing;
import javafx.scene.control.*;
import sttc.iso.input.ControlScheme;
import sttc.iso.input.Controller;
import zerfall.Main;
import zerfall.controls.ClassicControlScheme;
import zerfall.controls.DefaultControlScheme;

import java.net.URL;
import java.util.ResourceBundle;

/**
 *
 */
public class MainSceneController implements Initializable {
  @FXML
  private Button newGameButton;
  @FXML
  private Button optionsButton;
  @FXML
  private Button quitButton;

  @FXML
  private DialogPane optionPanel;
  @FXML
  private ChoiceBox windowSizeOption;
  @FXML
  private ChoiceBox controlSchemeOption;
  @FXML
  private CheckBox antialiasOption;


  @SuppressWarnings("unchecked")
  @Override
  public void initialize(URL path, ResourceBundle resources) {
    newGameButton.setOnAction(evt -> Main.newGame());
    optionsButton.setOnAction(evt -> optionPanel.setVisible(true));
    quitButton.setOnAction(evt -> Main.quit());

    optionPanel.setVisible(false);


    String version = System.getProperty("java.specification.version");
    String os = System.getProperty("os.name");
    if (version.equals("1.8") && os.equals("Mac OS X")) {
      antialiasOption.setDisable(true);
    }

    windowSizeOption.setItems(FXCollections.observableArrayList("640x360", "960x540", "1280x720", "1600x900", "1920x1080", "2560x1440"));
    windowSizeOption.setValue("1280x720");

    controlSchemeOption.setItems(FXCollections.observableArrayList("DEFAULT", "CLASSIC"));
    controlSchemeOption.setValue("DEFAULT");

    Button optionApplyButton = (Button) optionPanel.lookupButton(ButtonType.APPLY);
    optionApplyButton.setOnAction(evt -> applyOptions());
    optionApplyButton.setText("APPLY");

    Button optionCloseButton = (Button) optionPanel.lookupButton(ButtonType.CLOSE);
    optionCloseButton.setOnAction(evt -> optionPanel.setVisible(false));
    optionCloseButton.setText("CLOSE");

    applyOptions();
  }

  private void applyOptions() {
    SceneAntialiasing antialiasing;

    String[] dimensionsAsStrings = ((String) windowSizeOption.getValue()).split("x");
    int width = Integer.parseInt(dimensionsAsStrings[0]);
    int height = Integer.parseInt(dimensionsAsStrings[1]);

    ControlScheme scheme = null;
    if (controlSchemeOption.getValue().equals("DEFAULT")) {
      scheme = new DefaultControlScheme();
    } else if (controlSchemeOption.getValue().equals("CLASSIC")) {
      scheme = new ClassicControlScheme();
    }
    assert scheme != null;
    Controller.setControlScheme(scheme);

    if (antialiasOption.isSelected()) {
      antialiasing = SceneAntialiasing.BALANCED;
    } else {
      antialiasing = SceneAntialiasing.DISABLED;
    }

    Main.loadGameUI(width, height, antialiasing);
  }
}