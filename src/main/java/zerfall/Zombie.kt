package zerfall

import javafx.scene.canvas.GraphicsContext
import javafx.scene.shape.Shape
import sttc.iso.sprite.Sprite
import sttc.iso.world.Entity
import sttc.iso.world.Npc
import sttc.sound.AudioLoader
import sttc.sound.AudioSource
import sttc.sound.Sound
import sttc.util.Calculator
import sttc.util.geom.SimpleLine2D
import sttc.util.geom.SimplePoint2D
import sttc.util.shape.Rectangle2D
import java.io.Serializable

/**

 */
internal class Zombie(x: Double, y: Double, health: Double) : Npc(x, y, health), Serializable {
  private val audioSource = AudioSource()
  private var maxSpeed = 2.0
  private var atRest: Boolean = false
  private val sprite: Sprite = Sprite("sprites/Zombie")

  init {
    width = sprite.width.toDouble()
    height = sprite.height.toDouble()
    sprite.playAnim("idle", "normal", "left")
    timeScale = 1.0
  }

  override fun collide(shape: Shape) {
    atRest = false
    var hitCeiling = false
    var x = Calculator.round(this.x, 2)
    var y = Calculator.round(this.y, 2)
    var ib = Rectangle2D.convert(Shape.intersect(bounds, shape))
    var intersect = Rectangle2D(ib.min.x, ib.min.y, ib.width, ib.height)
    var bounds = Rectangle2D(x, y, width, height)
    while (!intersect.isEmpty) {
      if (Calculator.round(intersect.width, 2) > Calculator.round(intersect.height, 2)) {
        if (intersect.max.y == bounds.max.y || bounds.center.y < intersect.center.y) {
          y -= intersect.height
          atRest = true
        }
        if (intersect.min.y == bounds.min.y || bounds.center.y > intersect.center.y) {
          y += intersect.height
          hitCeiling = true
        }
      } else {
        if (intersect.max.x == bounds.max.x || bounds.center.x < intersect.center.x) {
          x -= intersect.width
        }
        if (intersect.min.x == bounds.min.x || bounds.center.x > intersect.center.x) {
          x += intersect.width
        }
      }
      bounds = Rectangle2D(x, y, width, height)
      ib = Rectangle2D.convert(Shape.intersect(bounds, shape))
      intersect = Rectangle2D(ib.min.x, ib.min.y, ib.width, ib.height)
    }
    setLocation(Math.round(x).toDouble(), Math.round(y).toDouble())
    if (atRest || hitCeiling) {
      if (yVelocity > 5) {
        yVelocity = -yVelocity / 4
      } else {
        yVelocity = 0.0
      }
    }
  }

  fun setMaxSpeed(speed: Double) {
    this.maxSpeed = speed
  }

  override fun damage(amount: Double) {
    health -= amount
    if (health <= 0) {
      idleSounds.forEach { sound -> audioSource.stop(sound) }
      runSounds.forEach { sound -> audioSource.stop(sound) }
      val rand = (Math.random() * deathSounds.size).toInt()
      audioSource.play(deathSounds[rand])
      target = null
      xVelocity = 0.0
    }
  }

  override fun movement() {
    if (target != null) {
      val nonNullTarget = target as Entity
      if (Shape.intersect(bounds, nonNullTarget.bounds).layoutBounds.isEmpty) {
        if (nonNullTarget.x < x) {
          moveLeft(1.0)
        } else {
          moveRight(1.0)
        }
        if (nonNullTarget.y < y - height) {
          jump(1.0)
        }
      }
    }
  }

  private fun moveLeft(multiplier: Double) {
    if (xVelocity > -maxSpeed * multiplier) {
      xVelocity -= 1 * multiplier
    }
  }

  private fun moveRight(multiplier: Double) {
    if (xVelocity < maxSpeed * multiplier) {
      xVelocity += 1 * multiplier
    }
  }

  private fun jump(multiplier: Double) {
    if (atRest) {
      yVelocity = -30 * multiplier
      atRest = false
    }
  }

  override fun visual() {
    if ((sprite.action == "jump" || sprite.action == "fall") && atRest) {
      sprite.action = "idle"
    }
    if (xVelocity < 0) {
      sprite.direction = "left"
    }
    if (xVelocity > 0) {
      sprite.direction = "right"
    }
    sprite.state = "normal"
  }

  override fun start() {
    // not necessary in this case
  }

  override fun finish() {
    // not necessary in this case
  }

  override fun render(g: GraphicsContext) {
    g.drawImage(sprite.get(), x, y)
  }

  override fun mechanics() {
    val b = idleSounds.any { s -> audioSource.isPlaying(s) } || runSounds.any { s -> audioSource.isPlaying(s) }
    if (Math.random() < 0.005 && !b) {
      var pan: Double
      pan = SimpleLine2D(player.bounds.center, bounds.center).signedWidth / 1280
      if (pan > 1) {
        pan = 1.0
      } else if (pan < -1) {
        pan = -1.0
      }
      val randIndex: Int
      if (maxSpeed < 4) {
        randIndex = (Math.random() * idleSounds.size).toInt()
        audioSource.setPan(idleSounds[randIndex], pan)
        audioSource.play(idleSounds[randIndex])
      } else {
        randIndex = (Math.random() * runSounds.size).toInt()
        audioSource.setPan(runSounds[randIndex], pan)
        audioSource.play(runSounds[randIndex])
      }
    }
  }

  override fun physics() {
    y += yVelocity * timeScale
    x += xVelocity * timeScale
    yVelocity += gravity * timeScale
    atRest = false
  }

  fun getMeleeTrajectory(): SimpleLine2D {
    val distance = 75
    val angle = if (sprite.direction == "left") Math.PI else 0.0
    val origin = bounds.center
    val end = SimplePoint2D(origin.x + distance * Math.cos(angle), origin.y)
    return SimpleLine2D(origin, end)
  }

  override fun copy(): Entity {
    val zombie = Zombie(x, y, health)
    zombie.gravity = gravity
    zombie.xVelocity = xVelocity
    zombie.yVelocity = yVelocity
    zombie.target = target
    zombie.maxSpeed = maxSpeed
    return zombie
  }

  companion object {
    private const val serialVersionUID = -6297035990145120410L
    private var deathSounds = mutableListOf<Sound>()
    private val idleSounds = mutableListOf<Sound>()
    private val runSounds = mutableListOf<Sound>()

    init {
      (0..9).mapTo(deathSounds) { AudioLoader.load("sounds/zombie/death/$it.au") }
      (0..5).mapTo(idleSounds) { AudioLoader.load("sounds/zombie/idle/$it.au") }
      (0..5).mapTo(runSounds) { AudioLoader.load("sounds/zombie/run/$it.au") }
    }
  }
}