package zerfall.controls

import sttc.iso.input.Action
import sttc.iso.input.ControlScheme
import sttc.util.input.Input

/**

 */
class DefaultControlScheme : ControlScheme("DEFAULT") {
  init {
    map(Action.MOVE_LEFT, Input.A)
    map(Action.MOVE_RIGHT, Input.D)
    map(Action.JUMP, Input.W)
    map(Action.SPRINT, Input.SHIFT)
    map(Action.SHOOT, Input.SPACE)
    map(Action.RELOAD, Input.R)
    map(Action.MELEE, Input.V)
    map(Action.USE, Input.F)
  }
}
