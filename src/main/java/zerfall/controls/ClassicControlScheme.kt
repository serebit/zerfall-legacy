package zerfall.controls

import sttc.iso.input.Action
import sttc.iso.input.ControlScheme
import sttc.util.input.Input

/**

 */
class ClassicControlScheme : ControlScheme("CLASSIC") {
  init {
    map(Action.MOVE_LEFT, Input.A)
    map(Action.MOVE_RIGHT, Input.D)
    map(Action.JUMP, Input.SPACE)
    map(Action.SPRINT, Input.SHIFT)
    map(Action.SHOOT, Input.L)
    map(Action.RELOAD, Input.R)
    map(Action.MELEE, Input.K)
    map(Action.USE, Input.F)
  }
}
