package zerfall

import javafx.scene.image.Image
import sttc.iso.weap.Gun
import sttc.iso.world.InteractiveItem
import sttc.iso.world.Layer
import sttc.sound.AudioLoader
import sttc.sound.AudioSource
import java.util.*

/**

 */
internal class MysteryBox(layer: Layer, x: Double, y: Double, vararg guns: Gun) : InteractiveItem(x, y, 160.0, 40.0,
    layer, Image("images/mystery_box.png")) {
  private val audioSource = AudioSource()
  private val cost = 950
  private val timer = Timer()
  private lateinit var spin: TimerTask
  private lateinit var close: TimerTask
  private var state = State.IDLE
  private var gun: Gun? = null
  private var guns = mutableListOf<Gun>()

  private val jingle = AudioLoader.load("sounds/mystery_box.au")
  private val buy = AudioLoader.load("sounds/cash_register.au")


  init {
    this.guns.addAll(guns)
    reset()
    dialog = "Press F to get random weapon (Cost: $cost)"
  }

  override fun runAction() {
    if (state == State.IDLE) {
      if (player.withdrawPoints(cost)) {
        timer.purge()
        audioSource.play(jingle)
        state = State.SPINNING
        timer.schedule(spin, 5000)
        timer.schedule(close, 12500)
        dialog = ""
      }
    }

    if (state == State.WAITING) {
      player.gun = gun as Gun
      gun = null
      audioSource.play(buy)
      spin.cancel()
      close.cancel()
      timer.purge()
      state = State.IDLE
      reset()
    }
  }

  private fun reset() {
    spin = object : TimerTask() {
      override fun run() {
        var gunToTake: Gun
        do {
          gunToTake = guns[(Math.random() * guns.size).toInt()].copy()
        } while(player.gun.sameModel(gunToTake))
        gun = gunToTake
        dialog = "Press F to take " + gunToTake.name
        state = State.WAITING
      }
    }
    close = object : TimerTask() {
      override fun run() {
        gun = null
        state = State.IDLE
        spin.cancel()
        reset()
      }
    }
    dialog = "Press F to get random weapon (Cost: $cost)"
  }

  private enum class State {
    IDLE, SPINNING, WAITING
  }

  companion object {
    lateinit var player: Character
  }
}
