package zerfall

import javafx.animation.AnimationTimer
import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.scene.SceneAntialiasing
import javafx.scene.canvas.Canvas
import javafx.scene.canvas.GraphicsContext
import javafx.scene.effect.GaussianBlur
import javafx.scene.effect.InnerShadow
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.StackPane
import javafx.scene.paint.Color
import javafx.scene.text.Font
import javafx.stage.Stage
import sttc.iso.world.Environment
import sttc.sound.Sound
import sttc.util.input.Input
import sttc.util.input.InputMap
import java.io.File

class Main : Application() {

  @Throws(Exception::class)
  override fun start(stage: Stage) {
    Main.stage = stage
    stage.setOnCloseRequest { quit() }
    stage.isResizable = false
    loadFonts()
    loadMainUI()
    loadGameUI(1280.0, 720.0, SceneAntialiasing.DISABLED)
    gameHandler = object : AnimationTimer() {
      override fun handle(now: Long) {
        val world = world!!
        if (InputMap.isPress(Input.ESCAPE)) {
          togglePaused()
        }
        if (!paused) {
          world.execute()
          world.render(gameSurface)
          if (world.readyForReset()) {
            newGame()
          }
        }
      }
    }
    stage.scene = mainMenuScene
    stage.show()
  }

  companion object {
    private lateinit var stage: Stage
    private lateinit var gameScene: Scene
    private lateinit var gameSurface: GraphicsContext
    private lateinit var mainMenuScene: Scene
    private var world: Environment? = null
    private var paused = false
    private lateinit var gameHandler: AnimationTimer

    @JvmStatic
    fun main(args: Array<String>) {
      launch(Main::class.java)
    }

    private fun loadMainUI() {
      val root = FXMLLoader.load<AnchorPane>(ClassLoader.getSystemResource("ui/main.fxml"))
      mainMenuScene = Scene(root, 1280.0, 720.0, false)
    }

    @JvmStatic fun loadGameUI(width: Double, height: Double, antialiasing: SceneAntialiasing) {
      val root = FXMLLoader.load<StackPane>(ClassLoader.getSystemResource("ui/game.fxml"))
      gameScene = Scene(root, width, height, true, antialiasing)
      InputMap.init(gameScene)
    }

    private fun loadFonts() {
      File(ClassLoader.getSystemResource("fonts").toURI()).listFiles().forEach {
        Font.loadFont(it.toURI().toURL().toExternalForm(), 10.0)
      }
    }

    @JvmStatic fun openMainMenu() {
      gameHandler.stop()
      world?.close()
      world = null
      val gameContainer = gameScene.lookup("#game") as StackPane
      gameContainer.children.clear()
      stage.scene = mainMenuScene
    }

    @JvmStatic fun newGame() {
      stage.scene = gameScene
      if (paused) {
        togglePaused()
      }
      val canvas = gameScene.lookup("#game-canvas") as Canvas
      canvas.width = gameScene.width
      canvas.height = gameScene.height
      gameSurface = canvas.graphicsContext2D
      gameSurface.scale(canvas.scene.width / 1280, canvas.scene.height / 720)
      world = PrototypeMap()
      gameHandler.start()
    }

    @JvmStatic fun togglePaused() {
      val pauseMenu = gameScene.lookup("#pause-menu") as AnchorPane
      val gamePane = gameScene.lookup("#game") as StackPane
      if (paused) {
        pauseMenu.isVisible = false
        gamePane.effect = null
        Sound.resume()
      } else {
        val blur = GaussianBlur(20.0)
        val shadow = InnerShadow(20.0, Color.BLACK)

        shadow.input = blur
        gamePane.effect = shadow
        Sound.pause()
      }
      paused = !paused
      pauseMenu.isVisible = paused
    }

    @JvmStatic fun quit() {
      stage.close()
      System.exit(0)
    }
  }
}