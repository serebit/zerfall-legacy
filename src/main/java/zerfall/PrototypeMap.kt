package zerfall

import javafx.geometry.Point2D
import javafx.scene.canvas.GraphicsContext
import javafx.scene.effect.BlendMode
import javafx.scene.image.Image
import javafx.scene.paint.Color
import javafx.scene.paint.Paint
import javafx.scene.shape.Shape
import javafx.scene.text.Font
import javafx.scene.text.TextAlignment
import sttc.iso.effect.Effect
import sttc.iso.world.*
import sttc.sound.AudioLoader
import sttc.sound.AudioSource
import sttc.util.geom.SimpleLine2D
import sttc.util.input.Input
import sttc.util.input.InputMap
import sttc.util.input.InputState
import sttc.util.shape.Line2D
import sttc.util.shape.Rectangle2D
import zerfall.weap.*
import java.util.*

internal class PrototypeMap : Environment(1.0) {
  override var items = mutableListOf<Item>()
  override var npcs = mutableListOf<Npc>()
  override var effectQueue = mutableListOf<Effect>()
  override var player: Player = Character(2560.0, 500.0)
  override var dialog = ""
  override var skybox: Paint = Color.DARKGRAY
  private val audioSource = AudioSource()
  private var round = 1
  private var zombiesLeft = 6
  private var zombiesSpawned = 0
  private val roundStart = AudioLoader.load("sounds/wave_change.au")

  init {
    Npc.player = player
    Door.player = player
    WallBuy.player = player as Character
    MysteryBox.player = player as Character

    val floor3room1spawner1 = Spawner(
        Layer.LAYER3,
        1990.0, 490.0, 100.0, 100.0,
        Image("images/spawn_window.png"),
        Zombie(1990.0, 390.0, 20.0),
        true
    )
    val floor3room1spawner2 = Spawner(
        Layer.LAYER3,
        3120.0, 490.0, 100.0, 100.0,
        Image("images/spawn_window.png"),
        Zombie(3120.0, 490.0, 20.0),
        true
    )
    val floor2room1spawner1 = Spawner(
        Layer.LAYER3,
        1990.0, 790.0, 100.0, 100.0,
        Image("images/spawn_window.png"),
        Zombie(1990.0, 790.0, 20.0),
        false
    )
    val floor2room2spawner1 = Spawner(
        Layer.LAYER3,
        3120.0, 790.0, 100.0, 100.0,
        Image("images/spawn_window.png"),
        Zombie(3120.0, 790.0, 20.0),
        false
    )
    val floor1room1spawner1 = Spawner(
        Layer.LAYER3,
        1990.0, 1090.0, 100.0, 100.0,
        Image("images/spawn_window.png"),
        Zombie(1990.0, 1090.0, 20.0),
        false
    )
    val floor1room2spawner1 = Spawner(
        Layer.LAYER3,
        3120.0, 1090.0, 100.0, 100.0,
        Image("images/spawn_window.png"),
        Zombie(3120.0, 1090.0, 20.0),
        false
    )

    // LAYER 2
    place(
        Item(Layer.LAYER2, 2555.0, 980.0, 10.0, 75.0),
        Item(Layer.LAYER2, 4470.0, 980.0, 10.0, 75.0),
        Item(Layer.LAYER2, 4030.0, 680.0, 10.0, 75.0),
        Item(Layer.LAYER2, 2555.0, 680.0, 10.0, 75.0),
        Item(Layer.LAYER2, 640.0, 980.0, 10.0, 75.0),
        Item(Layer.LAYER2, 1080.0, 680.0, 10.0, 75.0),
        Item(Layer.LAYER2, 1080.0, 655.0, 2960.0, 25.0),
        Item(Layer.LAYER2, 640.0, 955.0, 3840.0, 25.0),
        Item(Layer.LAYER2, 25.0, 1255.0, 5070.0, 25.0),
        Item(Layer.LAYER2, 5095.0, 0.0, 25.0, 1280.0),
        Item(Layer.LAYER2, 0.0, 0.0, 25.0, 1280.0),
        Door(
            4030.0, 755.0, 10.0, 200.0, Image("images/door.png"), "Press F to open door",
            750, AudioLoader.load("sounds/door_open.au"),
            floor2room2spawner1
        ),
        Door(
            2555.0, 755.0, 10.0, 200.0, Image("images/door.png"), "Press F to open door",
            750, AudioLoader.load("sounds/door_open.au"),
            floor2room1spawner1, floor2room2spawner1
        ),
        Door(
            1080.0, 755.0, 10.0, 200.0, Image("images/door.png"), "Press F to open door",
            750, AudioLoader.load("sounds/door_open.au"),
            floor2room1spawner1
        ),
        Door(
            4470.0, 1055.0, 10.0, 200.0, Image("images/door.png"), "Press F to open door",
            750, AudioLoader.load("sounds/door_open.au"),
            floor1room2spawner1
        ),
        Door(
            2555.0, 1055.0, 10.0, 200.0, Image("images/door.png"), "Press F to open door",
            750, AudioLoader.load("sounds/door_open.au"),
            floor1room1spawner1, floor2room2spawner1
        ),
        Door(
            640.0, 1055.0, 10.0, 200.0, Image("images/door.png"), "Press F to open door",
            750, AudioLoader.load("sounds/door_open.au"),
            floor1room1spawner1
        )
    )

    // LAYER 3
    place(
        MysteryBox(Layer.LAYER3, 1100.0, 1215.0,
            Aug(),
            Gewehr(),
            Stoner(),
            Ak47(),
            Famas(),
            Dragunov(),
            Fal(),
            M60(),
            Thompson(),
            Rpk(),
            ScarH(),
            Ppsh()
        ),
        floor3room1spawner1,
        floor3room1spawner2,
        floor2room1spawner1,
        floor2room2spawner1,
        floor1room1spawner1,
        floor1room2spawner1,
        WallBuy(
            Layer.LAYER3,
            1525.0, 554.0, 50.0, 32.0, 500,
            Mac11()
        ),
        WallBuy(
            Layer.LAYER3,
            3665.0, 553.0, 50.0, 34.0, 500,
            M1911()
        ),
        WallBuy(
            Layer.LAYER3,
            3010.0, 1144.0, 100.0, 52.0, 1250,
            Mp5()
        ),
        WallBuy(
            Layer.LAYER3,
            1596.0, 840.0, 88.0, 50.0, 1000,
            Mp40()
        ),
        WallBuy(
            Layer.LAYER3,
            3530.0, 840.0, 100.0, 57.0, 1250,
            Ump45()
        )
    )

    // LAYER 4
    place(Item(
        Layer.LAYER4,
        0.0, 0.0, 5120.0, 1280.0,
        Image("images/metal_wall_dirt.png", 5120.0, 1280.0, false, true)
    ))
    spawnPlayer(player)
  }

  @Suppress("UNCHECKED_CAST")
  @Synchronized override fun execute() {
    var itemCollected = false
    val npcs = npcs.toMutableSet()
    val items = items.toMutableSet()
    val player = player as Character
    player.start()
    player.execute()
    if (player.isShooting()) {
      val hitThing = castRay(player.getBulletTrajectory(), Layer.LAYER2, player)
      if (hitThing != null) {
        if (hitThing is Npc) {
          hitThing.damage(player.gun.DAMAGE.toDouble())
          player.depositPoints(10)
        }
        val line = Line2D(
            player.getBulletTrajectory().start.toFx,
            Point2D(hitThing.bounds.center.x, player.getBulletTrajectory().start.y))
        line.stroke = Color.ORANGE
        line.strokeWidth = 3.0
        line.opacity = 0.75
        effectQueue.add(Effect(Layer.LAYER2, line.bisect(Line2D.Section.START), line.bisect(Line2D.Section.END)))
      }
    }
    if (player.isKnifing) {
      val meleeTrajectory = player.getMeleeTrajectory()
      val hitThing = castRay(meleeTrajectory, Layer.LAYER2, player)
      if (hitThing is Npc) {
        hitThing.damage(25.0)
        player.depositPoints(70)
      }
    }

    for (npc in npcs.toMutableSet()) {
      npc.execute()
      if (npc is Zombie) {
        if (castRay(npc.getMeleeTrajectory(), Layer.LAYER2, npc) == player) {
          player.damage(3.0)
        }
      }
      npc.visual()
    }

    val fKey = InputMap.getState(Input.F)

    dialog = ""

    items.toMutableSet().forEach { item ->
      when (item) {
        is InteractiveItem -> {
          val bounds = item.bounds
          bounds.scale(Rectangle2D.Origin.CENTER, 20.0, 20.0)
          val playerBounds = player.bounds
          val intersect = Rectangle2D.convert(Shape.intersect(playerBounds, item.bounds))
          if (!intersect.isEmpty) {
            dialog = item.dialog
            if (fKey == InputState.RELEASE) {
              item.runAction()
            }
          }
          bounds.scale(Rectangle2D.Origin.CENTER, -20.0, -20.0)
        }
        is Spawner -> if (zombiesLeft > 0 && zombiesSpawned <= 12) {
          if (Math.random() < 0.005) {
            val e = item.spawn()
            if (e is Zombie) {
              e.target = player
              zombiesLeft--
              zombiesSpawned++
              val timer = Timer()
              timer.schedule(object : TimerTask() {
                override fun run() {
                  spawn(e)
                }
              }, 5000)
            }
          }
        }
        is CollectableItem -> {
          val playerBounds = player.bounds
          val intersect = Rectangle2D.convert(Shape.intersect(playerBounds, item.bounds))
          if (!intersect.isEmpty) {
            dialog = item.dialog
            if (fKey == InputState.RELEASE && !itemCollected) {
              item.collect()
              if (item.collected) {
                items.remove(item)
                itemCollected = true
              }
            }
          }
        }
      }

      if (item.layer == Layer.LAYER2) {
        npcs.forEach { entity ->
          val entityBounds = entity.bounds
          val intersect = Rectangle2D.convert(Shape.intersect(entityBounds, item.bounds))
          if (!intersect.isEmpty) {
            entity.collide(item.bounds)
          }
        }
        val playerBounds = player.bounds
        val intersect = Rectangle2D.convert(Shape.intersect(playerBounds, item.bounds))
        if (!intersect.isEmpty) {
          player.collide(item.bounds)
        }
      }
    }

    player.visual()
    player.finish()
    npcs.toMutableSet().forEach(Npc::visual)
    npcs.toMutableSet().forEach(Npc::finish)
    if (zombiesLeft <= 0 && zombiesSpawned <= 0) {
      advanceRound()
    }
    this.items = items.toMutableList()
    purge()
  }

  @Synchronized fun purge() {
    val npcsToRemove = mutableListOf<Npc>()
    npcs.toMutableSet().forEach { npc ->
      if (npc.health <= 0) {
        npcsToRemove.add(npc)
        if (npc is Zombie) {
          zombiesSpawned--
        }
        player.depositPoints(50)
      }
    }
    npcs.removeAll(npcsToRemove)
  }

  override fun render(surface: GraphicsContext) {
    val debug = false
    val xTranslate = -player.x + 1280 / 2 - player.bounds.width / 2
    val yTranslate = -player.y + 720 / 2 - player.bounds.height / 2

    surface.clearRect(0.0, 0.0, 1280.0, 720.0)
    surface.translate(xTranslate, yTranslate)
    Layer.values().forEach { layer ->
      filterItems(layer).forEach { item -> item.render(surface) }
      if (layer == Layer.LAYER2) {
        player.render(surface)
        npcs.forEach { npc -> npc.render(surface) }
      }
      filterEffects(layer).forEach { effect -> effect.render(surface) }
    }
    if (debug) {
      npcs.toMutableSet().forEach { npc ->
        val ray = getCastRay(SimpleLine2D(player.bounds.center, npc.bounds.center), Layer.LAYER2).toFx
        ray.stroke = Color.RED
        ray.strokeWidth = 2.0
        ray.opacity = 1.0
        ray.render(surface)
      }
    }
    surface.translate(-xTranslate, -yTranslate)
    renderHud(surface)
    purgeEffects()
  }

  private fun renderHud(surface: GraphicsContext) {
    surface.save()

    // Gun info
    surface.fill = Color.LIGHTGRAY
    surface.font = Font.font("RM Typerighter old", 38.0)
    surface.textAlign = TextAlignment.RIGHT
    surface.fillText(player.gun.name, 1260.0, 640.0, 160.0)
    surface.font = Font.font("RM Typerighter old", 96.0)
    surface.fillText(player.gun.clip.toString() + "/" + player.gun.cache, 1260.0, 700.0, 256.0)

    // Player points
    surface.textAlign = TextAlignment.LEFT
    surface.font = Font.font("Brother Deluxe 1350 Font", 24.0)
    surface.fillText((player as Character).points.toString(), 20.0, 620.0, 192.0)

    // Dialog bar
    surface.textAlign = TextAlignment.CENTER
    surface.font = Font.font("RM Typerighter old", 48.0)
    surface.fillText(dialog, 640.0, 540.0, 640.0)

    // Current round
    surface.textAlign = TextAlignment.LEFT
    surface.font = Font.font("Brother Deluxe 1350 Font", 64.0)
    surface.fill = Color.BLACK
    surface.globalAlpha = 0.5
    surface.fillText(round.toString(), 22.0, 688.0, 192.0)
    surface.fill = Color.RED
    surface.globalAlpha = 1.0
    surface.fillText(round.toString(), 20.0, 690.0, 192.0)

    // Health bar
    surface.globalBlendMode = BlendMode.COLOR_DODGE
    surface.globalAlpha = 1.0
    surface.fill = Color.DARKRED
    surface.fillRect(515 + player.health, 680.0, 250 - player.health, 25.0)
    surface.fill = Color.FORESTGREEN
    surface.fillRect(515.0, 680.0, player.health, 25.0)
    surface.globalAlpha = 1.0
    surface.globalBlendMode = BlendMode.SRC_OVER

    // Restore previous attributes
    surface.restore()
  }

  private fun advanceRound() {
    round++
    audioSource.stop(roundStart)
    audioSource.play(roundStart)
    zombiesLeft = Math.round(12 * Math.pow(1.1, round.toDouble()) - 6).toInt()
    val zombieSpeed: Double
    if (round < 10) {
      zombieSpeed = (round / 2 + 1).toDouble()
    } else {
      zombieSpeed = 5.0
    }
    val zombieHealth: Double = when {
      (round < 10) -> (20 + 15 * (round - 1)).toDouble()
      else -> 155 * Math.pow(1.1, (round - 9).toDouble())
    }
    for (item in filterItems(Spawner::class.java)) {
      val spawner = item
      if (spawner.entityClass == Zombie::class.java) {
        val z = Zombie(0.0, 0.0, zombieHealth)
        z.setMaxSpeed(zombieSpeed + (Math.random() - 0.5) * 3)
        spawner.setEntity(z)
      }
    }
  }
}
