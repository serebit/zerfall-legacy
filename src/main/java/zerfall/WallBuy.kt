package zerfall

import sttc.iso.weap.Gun
import sttc.iso.world.InteractiveItem
import sttc.iso.world.Layer
import sttc.sound.AudioLoader
import sttc.sound.AudioSource

/**

 */
internal class WallBuy(layer: Layer, x: Double, y: Double, width: Double, height: Double, private val cost: Int,
                       private val gun: Gun) : InteractiveItem(x, y, width, height, layer) {
  val audioSource = AudioSource()
  override var dialog = ""
    get() {
      if (gun.sameModel(player.gun)) {
        val cost = cost / 2
        return "Press F to buy ammo (Cost: $cost)"
      } else {
        return "Press F to buy " + gun.name + " (Cost: $cost)"
      }
    }

  init {
    dialog = "Press F to buy " + gun.name + " (Cost: $cost)"
    texture = gun.outline
  }

  override fun runAction() {
    var cost = cost
    when {
      gun.sameModel(player.gun) -> if (player.withdrawPoints(cost / 2)) {
        player.gun.cache = player.gun.CACHE_SIZE
        audioSource.play(buy)
      }
      else -> if (player.withdrawPoints(cost)) {
        player.gun = gun.copy()
        audioSource.play(buy)
      }
    }
  }

  companion object {
    lateinit var player: Character
    val buy = AudioLoader.load("sounds/cash_register.au")
  }
}
