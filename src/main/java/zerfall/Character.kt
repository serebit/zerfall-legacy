package zerfall

import javafx.scene.canvas.GraphicsContext
import javafx.scene.shape.Shape
import sttc.iso.input.Action
import sttc.iso.input.ActionState
import sttc.iso.input.Controller
import sttc.iso.sprite.Sprite
import sttc.iso.weap.Gun
import sttc.iso.world.Entity
import sttc.iso.world.Player
import sttc.sound.AudioLoader
import sttc.sound.AudioSource
import sttc.util.Calculator
import sttc.util.geom.SimpleLine2D
import sttc.util.geom.SimplePoint2D
import sttc.util.shape.Rectangle2D
import zerfall.weap.Ppk

internal class Character(x: Double, y: Double) : Player(x, y, 250.0) {
  val audioSource = AudioSource()
  var points = 0
    private set
  private var atRest = false
  override var gun: Gun = Ppk()
  private val knifeSound = AudioLoader.load("sounds/knife_swing.au")
  private val sprite = Sprite("sprites/Player")

  init {
    width = sprite.width.toDouble()
    height = sprite.height.toDouble()
    sprite.playAnim("idle", "normal", "left")
    gun.cache = 32
    timeScale = 1.0
    points = 500
  }

  override fun collide(shape: Shape) {
    atRest = false
    var hitCeiling = false
    var x = Calculator.round(x, 2)
    var y = Calculator.round(y, 2)
    var intersect = Rectangle2D.convert(Shape.intersect(bounds, shape))
    var bounds = Rectangle2D(x, y, width, height)
    while (!intersect.isEmpty) {
      // if the intersect height is greater than the width, or is equivalent
      if (Calculator.round(intersect.width, 2) > Calculator.round(intersect.height, 2)) {
        if (intersect.max.y == bounds.max.y || bounds.center.y < intersect.center.y) {
          y -= intersect.height
          atRest = true
        }
        if (intersect.min.y == bounds.min.y || bounds.center.y > intersect.center.y) {
          y += intersect.height
          hitCeiling = true
        }
      } else {
        if (intersect.max.x == bounds.max.x || bounds.center.x < intersect.center.x) {
          x -= intersect.width
        }
        if (intersect.min.x == bounds.min.x || bounds.center.x > intersect.center.x) {
          x += intersect.width
        }
      }
      bounds = Rectangle2D(x, y, width, height)
      intersect = Rectangle2D.convert(Shape.intersect(bounds, shape))
    }
    setLocation(Math.round(x).toDouble(), Math.round(y).toDouble())
    if (atRest || hitCeiling) {
      if (yVelocity > 5) {
        yVelocity = -yVelocity / 4
      } else {
        yVelocity = 0.0
      }
    }
  }

  override fun movement() {
    var multiplier = 1
    if (Controller.isActive(Action.SPRINT)) {
      multiplier = 2
    }
    if (Controller.isActive(Action.MOVE_RIGHT)) {
      moveRight(multiplier / (1 + gun.WEIGHT / 10))
    } else if (Controller.isInactive(Action.MOVE_RIGHT) && xVelocity > 0) {
      if (Math.abs(xVelocity) > 1) {
        xVelocity -= 1.0
      } else {
        xVelocity = 0.0
      }
    }
    when {
      Controller.isActive(Action.MOVE_LEFT) -> moveLeft(multiplier / (1 + gun.WEIGHT / 10))
      Controller.isInactive(Action.MOVE_LEFT) && xVelocity < 0 -> if (Math.abs(xVelocity) > 1) {
        xVelocity += 1.0
      } else {
        xVelocity = 0.0
      }
    }
    if (Controller.isActive(Action.JUMP)) {
      jump(1.0)
    }
  }

  private fun moveLeft(multiplier: Double) {
    if (xVelocity > -10 * multiplier) {
      xVelocity -= 1 * multiplier
    }
  }

  private fun moveRight(multiplier: Double) {
    if (xVelocity < 10 * multiplier) {
      xVelocity += 1 * multiplier
    }
  }

  private fun jump(multiplier: Double) {
    if (atRest) {
      sprite.action = "jump"
      yVelocity = -27 * multiplier
      atRest = false
    }
  }

  override fun visual() {
    if ((sprite.action == "jump" || sprite.action == "fall") && atRest) {
      sprite.action = "idle"
    }
    when {
      xVelocity < 0 -> sprite.direction = "left"
      xVelocity > 0 -> sprite.direction = "right"
    }
    when {
      gun.state == Gun.State.FIRING -> sprite.state = "shooting"
      else -> sprite.state = "normal"
    }
  }

  override fun mechanics() {
    when (Controller.getState(Action.SHOOT)) {
      ActionState.START -> gun.pull()
      ActionState.END -> gun.release()
    }

    if (Controller.isStart(Action.RELOAD)) {
      gun.reload()
    }

    heal(0.5)
  }

  override fun physics() {
    y += yVelocity * timeScale
    x += xVelocity * timeScale
    yVelocity += gravity * timeScale
    atRest = false
  }

  override fun start() {
    gun.engage()
  }

  override fun finish() {
    gun.cycle()
  }

  override fun damage(amount: Double) {
    health -= amount
  }

  override fun depositPoints(amount: Int) {
    this.points += amount
  }

  override fun withdrawPoints(amount: Int): Boolean {
    if (points < amount) {
      return false // Withdrawal failed
    } else {
      this.points -= amount
      return true // Withdrawal succeeded
    }
  }

  override fun isShooting() = gun.state == Gun.State.FIRING

  val isKnifing: Boolean
    get() {
      if (Controller.isStart(Action.MELEE)) {
        audioSource.play(knifeSound)
        return true
      }
      return false
    }

  override fun getBulletTrajectory(): SimpleLine2D {
    val distance = 8000.0
    val angle = Math.toRadians((if (sprite.direction == "left") 180 else 0).toDouble())
    var startx = 0.0
    when {
      sprite.direction == "left" -> startx = bounds.min.x + 2
      sprite.direction == "right" -> startx = bounds.max.x - 2
    }
    val origin = SimplePoint2D(startx, bounds.center.y - 5)
    val end = SimplePoint2D(origin.x + distance * Math.cos(angle), origin.y + distance * Math.sin(angle))
    return SimpleLine2D(origin, end)
  }

  fun getMeleeTrajectory(): SimpleLine2D {
    val distance = 150
    val angle = if (sprite.direction == "left") Math.PI else 0.0
    val origin = bounds.center
    val end = SimplePoint2D(origin.x + distance * Math.cos(angle), origin.y)
    return SimpleLine2D(origin, end)
  }

  override fun render(g: GraphicsContext) {
    g.drawImage(sprite.get(), x, y, width, height)
  }

  override fun copy(): Entity {
    val character = Character(x, y)
    character.health = health
    character.gravity = gravity
    character.gun = gun
    character.xVelocity = xVelocity
    character.yVelocity = yVelocity
    character.points = points
    return character
  }
}