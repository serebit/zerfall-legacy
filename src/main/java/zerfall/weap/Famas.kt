package zerfall.weap

import sttc.iso.weap.AutoGun
import sttc.sound.AudioLoader

/**

 */
class Famas : AutoGun(
    name = "FAMAS F1",
    CLIP_SIZE = 25,
    CACHE_SIZE = 240,
    RATE_OF_FIRE = 950,
    RELOAD_TIME = 2250,
    DAMAGE = 45,
    WEIGHT = 3.61,
    fire = AudioLoader.load("guns/famas/fire.au"),
    reload = AudioLoader.load("guns/famas/reload.au"),
    outline = null
)
