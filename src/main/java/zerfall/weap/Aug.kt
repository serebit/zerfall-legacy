package zerfall.weap

import sttc.iso.weap.AutoGun
import sttc.sound.AudioLoader

/**

 */
class Aug : AutoGun(
    name = "AUG",
    CLIP_SIZE = 30,
    CACHE_SIZE = 240,
    RATE_OF_FIRE = 715,
    RELOAD_TIME = 2400,
    DAMAGE = 46,
    WEIGHT = 3.6,
    fire = AudioLoader.load("guns/aug/fire.au"),
    reload = AudioLoader.load("guns/aug/reload.au"),
    outline = null
)
