package zerfall.weap

import javafx.scene.image.Image
import sttc.iso.weap.AutoGun
import sttc.sound.AudioLoader

/**
 * Created by campbell on 1/2/17.
 */
class Mac11 : AutoGun(
    name = "MAC-11",
    CLIP_SIZE = 16,
    CACHE_SIZE = 144,
    RATE_OF_FIRE = 1200,
    RELOAD_TIME = 2000,
    DAMAGE = 17,
    WEIGHT = 1.59,
    fire = AudioLoader.load("guns/mac11/fire.au"),
    reload = AudioLoader.load("guns/mac11/reload.au"),
    outline = Image("guns/mac11/outline.png")
)