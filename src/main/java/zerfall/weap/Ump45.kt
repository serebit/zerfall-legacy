package zerfall.weap

import javafx.scene.image.Image
import sttc.iso.weap.AutoGun
import sttc.sound.AudioLoader

/**

 */
class Ump45 : AutoGun(
    name = "UMP-45",
    CLIP_SIZE = 25,
    CACHE_SIZE = 225,
    RATE_OF_FIRE = 600,
    RELOAD_TIME = 2500,
    DAMAGE = 63,
    WEIGHT = 2.65,
    fire = AudioLoader.load("guns/ump45/fire.au"),
    reload = AudioLoader.load("guns/ump45/reload.au"),
    outline = Image("guns/ump45/outline.png")
)
