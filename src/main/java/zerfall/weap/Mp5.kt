package zerfall.weap

import javafx.scene.image.Image
import sttc.iso.weap.AutoGun
import sttc.sound.AudioLoader

/**

 */
class Mp5 : AutoGun(
    name = "MP5A5",
    CLIP_SIZE = 30,
    CACHE_SIZE = 210,
    RATE_OF_FIRE = 800,
    RELOAD_TIME = 2400,
    DAMAGE = 38,
    WEIGHT = 3.1,
    fire = AudioLoader.load("guns/mp5/fire.au"),
    reload = AudioLoader.load("guns/mp5/reload.au"),
    outline = Image("guns/mp5/outline.png")
)