package zerfall.weap

import sttc.iso.weap.AutoGun
import sttc.sound.AudioLoader

/**

 */
class Ak47 : AutoGun(
    name = "AK-47",
    CLIP_SIZE = 30,
    CACHE_SIZE = 240,
    RATE_OF_FIRE = 600,
    RELOAD_TIME = 2600,
    DAMAGE = 92,
    WEIGHT = 3.47,
    fire = AudioLoader.load("guns/ak47/fire.au"),
    reload = AudioLoader.load("guns/ak47/reload.au"),
    outline = null
)
