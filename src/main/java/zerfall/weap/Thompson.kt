package zerfall.weap

import sttc.iso.weap.AutoGun
import sttc.sound.AudioLoader

/**

 */
class Thompson : AutoGun(
    name = "M1927",
    CLIP_SIZE = 50,
    CACHE_SIZE = 250,
    RATE_OF_FIRE = 750,
    RELOAD_TIME = 2400,
    DAMAGE = 63,
    WEIGHT = 4.8,
    fire = AudioLoader.load("guns/thompson/fire.au"),
    reload = AudioLoader.load("guns/thompson/reload.au"),
    outline = null
)