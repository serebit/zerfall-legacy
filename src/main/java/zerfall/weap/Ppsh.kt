package zerfall.weap

import sttc.iso.weap.AutoGun
import sttc.sound.AudioLoader

/**

 */
class Ppsh : AutoGun(
    name = "PPSh-41",
    CLIP_SIZE = 71,
    CACHE_SIZE = 284,
    RATE_OF_FIRE = 900,
    RELOAD_TIME = 2500,
    DAMAGE = 32,
    WEIGHT = 3.63,
    fire = AudioLoader.load("guns/ppsh/fire.au"),
    reload = AudioLoader.load("guns/ppsh/reload.au"),
    outline = null
)