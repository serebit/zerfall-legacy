package zerfall.weap

import javafx.scene.image.Image
import sttc.iso.weap.SemiGun
import sttc.sound.AudioLoader

/**

 */
class M1911 : SemiGun(
    name = "M1911",
    CLIP_SIZE = 7,
    CACHE_SIZE = 84,
    RELOAD_TIME = 1550,
    DAMAGE = 49,
    WEIGHT = 1.1,
    fire = AudioLoader.load("guns/m1911/fire.au"),
    reload = AudioLoader.load("guns/m1911/reload.au"),
    outline = Image("guns/m1911/outline.png")
)