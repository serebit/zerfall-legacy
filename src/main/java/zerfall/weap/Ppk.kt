package zerfall.weap

import sttc.iso.weap.SemiGun
import sttc.sound.AudioLoader

/**

 */
class Ppk : SemiGun(
    name = "PPK",
    CLIP_SIZE = 8,
    CACHE_SIZE = 80,
    RELOAD_TIME = 1300,
    DAMAGE = 6,
    WEIGHT = 0.67,
    fire = AudioLoader.load("guns/ppk/fire.au"),
    reload = AudioLoader.load("guns/ppk/reload.au"),
    outline = null
)