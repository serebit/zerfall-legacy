package zerfall.weap

import sttc.iso.weap.AutoGun
import sttc.sound.AudioLoader

/**

 */
class M60 : AutoGun(
    name = "M60",
    CLIP_SIZE = 100,
    CACHE_SIZE = 300,
    RATE_OF_FIRE = 575,
    RELOAD_TIME = 8800,
    DAMAGE = 175,
    WEIGHT = 10.5,
    fire = AudioLoader.load("guns/m60/fire.au"),
    reload = AudioLoader.load("guns/m60/reload.au"),
    outline = null
)
