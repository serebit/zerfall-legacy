package zerfall.weap

import sttc.iso.weap.AutoGun
import sttc.sound.AudioLoader

/**

 */
class Gewehr : AutoGun(
    name = "G3A4",
    CLIP_SIZE = 20,
    CACHE_SIZE = 240,
    RATE_OF_FIRE = 500,
    RELOAD_TIME = 2400,
    DAMAGE = 154,
    WEIGHT = 4.7,
    fire = AudioLoader.load("guns/gewehr/fire.au"),
    reload = AudioLoader.load("guns/gewehr/reload.au"),
    outline = null
)
