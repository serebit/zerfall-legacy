package zerfall.weap

import sttc.iso.weap.SemiGun
import sttc.sound.AudioLoader

class Dragunov : SemiGun(
    name = "Dragunov",
    CLIP_SIZE = 10,
    CACHE_SIZE = 60,
    RELOAD_TIME = 3000,
    DAMAGE = 178,
    WEIGHT = 4.3,
    fire = AudioLoader.load("guns/dragunov/fire.au"),
    reload = AudioLoader.load("guns/dragunov/reload.au"),
    outline = null
)
