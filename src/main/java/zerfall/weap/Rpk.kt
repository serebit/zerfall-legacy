package zerfall.weap

import sttc.iso.weap.AutoGun
import sttc.sound.AudioLoader

/**

 */
class Rpk : AutoGun(
    name = "RPK",
    CLIP_SIZE = 75,
    CACHE_SIZE = 300,
    RATE_OF_FIRE = 600,
    RELOAD_TIME = 4000,
    DAMAGE = 100,
    WEIGHT = 4.8,
    fire = AudioLoader.load("guns/rpk/fire.au"),
    reload = AudioLoader.load("guns/rpk/reload.au"),
    outline = null
)
