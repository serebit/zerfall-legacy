package zerfall.weap

import javafx.scene.image.Image
import sttc.iso.weap.SemiGun
import sttc.sound.AudioLoader

/**

 */
class M9A1 : SemiGun(
    name = "M9A1",
    CLIP_SIZE = 10,
    CACHE_SIZE = 100,
    RELOAD_TIME = 1300,
    DAMAGE = 34,
    WEIGHT = 0.95,
    fire = AudioLoader.load("guns/m9a1/fire.au"),
    reload = AudioLoader.load("guns/m9a1/reload.au"),
    outline = Image("guns/m9a1/outline.png")
)