package zerfall.weap

import javafx.scene.image.Image
import sttc.iso.weap.AutoGun
import sttc.sound.AudioLoader

/**

 */
class Mp40 : AutoGun(
    name = "MP40",
    CLIP_SIZE = 32,
    CACHE_SIZE = 192,
    RATE_OF_FIRE = 500,
    RELOAD_TIME = 2300,
    DAMAGE = 34,
    WEIGHT = 4.18,
    fire = AudioLoader.load("guns/mp40/fire.au"),
    reload = AudioLoader.load("guns/mp40/reload.au"),
    outline = Image("guns/mp40/outline.png")
)
