package zerfall.weap

import sttc.iso.weap.AutoGun
import sttc.sound.AudioLoader

/**

 */
class Stoner : AutoGun(
    name = "Stoner 63",
    CLIP_SIZE = 75,
    CACHE_SIZE = 225,
    RATE_OF_FIRE = 850,
    RELOAD_TIME = 2400,
    DAMAGE = 50,
    WEIGHT = 5.3,
    fire = AudioLoader.load("guns/stoner/fire.au"),
    reload = AudioLoader.load("guns/stoner/reload.au"),
    outline = null
)