package zerfall.weap

import javafx.scene.image.Image
import sttc.iso.weap.AutoGun
import sttc.sound.AudioLoader

/**

 */
class Uzi : AutoGun(
    name = "FAMAS F1",
    CLIP_SIZE = 32,
    CACHE_SIZE = 192,
    RATE_OF_FIRE = 600,
    RELOAD_TIME = 2600,
    DAMAGE = 38,
    WEIGHT = 3.5,
    fire = AudioLoader.load("guns/uzi/fire.au"),
    reload = AudioLoader.load("guns/uzi/reload.au"),
    outline = Image("guns/uzi/outline.png")
)