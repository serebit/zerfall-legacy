package zerfall.weap

import sttc.iso.weap.AutoGun
import sttc.sound.AudioLoader

/**

 */
class ScarH : AutoGun(
    name = "SCAR-H",
    CLIP_SIZE = 20,
    CACHE_SIZE = 240,
    RATE_OF_FIRE = 625,
    RELOAD_TIME = 2600,
    DAMAGE = 160,
    WEIGHT = 3.58,
    fire = AudioLoader.load("guns/scarh/fire.au"),
    reload = AudioLoader.load("guns/scarh/reload.au"),
    outline = null
)