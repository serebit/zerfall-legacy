package zerfall.weap

import sttc.iso.weap.SemiGun
import sttc.sound.AudioLoader

/**

 */
class Fal : SemiGun(
    name = "FAL",
    CLIP_SIZE = 20,
    CACHE_SIZE = 120,
    RELOAD_TIME = 2000,
    DAMAGE = 169,
    WEIGHT = 4.3,
    fire = AudioLoader.load("guns/fal/fire.au"),
    reload = AudioLoader.load("guns/fal/reload.au"),
    outline = null
)
