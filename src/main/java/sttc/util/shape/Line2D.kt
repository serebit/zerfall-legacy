package sttc.util.shape

import javafx.geometry.Point2D
import javafx.scene.canvas.GraphicsContext
import javafx.scene.shape.Line
import sttc.util.Calculator

/**

 */
class Line2D : Line, Shape2D {

  constructor(start: Point2D, end: Point2D) : super(start.x, start.y, end.x, end.y)

  constructor(x: Double, y: Double, x1: Double, y1: Double) : super(x, y, x1, y1)

  val start = Point2D(startX, startY)
  val end = Point2D(endX, endY)
  val midpoint = Point2D(Calculator.avg(start.x, end.x), Calculator.avg(start.y, end.y))

  val width = Math.abs(endX - startX)
  val signedWidth = endX - startX
  val height = Math.abs(endY - startY)
  val signedHeight = endY - startY
  val length = Math.sqrt(Calculator.pwr(width, 2) + Calculator.pwr(height, 2))

  fun bisect(): MutableList<Line2D> {
    return mutableListOf(Line2D(start, midpoint), Line2D(midpoint, end))
  }

  fun bisect(section: Section): Line2D {
    val line: Line2D
    if (section == Section.START) {
      line = Line2D(start, midpoint)
    } else if (section == Section.END) {
      line = Line2D(midpoint, end)
    } else {
      throw IllegalArgumentException()
    }
    line.fill = fill
    line.stroke = stroke
    line.blendMode = blendMode
    line.opacity = opacity
    line.strokeWidth = strokeWidth
    return line
  }

  fun split(numSections: Int): MutableList<Line2D> {
    val list = mutableListOf<Line2D>()
    val sectionXDiff = (end.x - start.x) / numSections
    val sectionYDiff = (end.y - start.x) / numSections
    var xValue = start.x
    var yValue = start.y
    for (i in (0..numSections - 1)) {
      list.add(Line2D(xValue, yValue, xValue + sectionXDiff, yValue + sectionYDiff))
      xValue += sectionXDiff
      yValue += sectionYDiff
    }
    return list
  }

  override fun render(g: GraphicsContext) {
    g.save()
    g.fill = fill
    g.stroke = stroke
    g.globalBlendMode = blendMode
    g.globalAlpha = opacity
    g.lineWidth = strokeWidth
    g.strokeLine(startX, startY, endX, endY)
    g.restore()
  }

  enum class Section {
    START, END
  }
}
