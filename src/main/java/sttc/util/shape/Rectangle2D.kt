package sttc.util.shape

import javafx.scene.canvas.GraphicsContext
import javafx.scene.shape.Rectangle
import javafx.scene.shape.Shape
import sttc.util.Calculator
import sttc.util.geom.SimplePoint2D

/**
 * Extended Rectangle class for mathematical calculations.
 */
class Rectangle2D : Rectangle, Shape2D {
  constructor(x: Double, y: Double, width: Double, height: Double) : super(x, y, width, height)

  val min = SimplePoint2D(x, y)
  val max = SimplePoint2D(x + width, y + height)
  val center = SimplePoint2D(Calculator.avg(min.x, max.x), Calculator.avg(min.y, max.y))
  val isEmpty = width <= 0 && height <= 0

  fun scale(origin: Origin, widthDiff: Double, heightDiff: Double) {
    when (origin) {
      Origin.TOP_LEFT -> {
        width += widthDiff
        height += heightDiff
      }
      Origin.TOP_RIGHT -> {
        x -= widthDiff
        width = x + widthDiff
        height = x + heightDiff
      }
      Origin.BOTTOM_LEFT -> {
        y -= heightDiff
        width += widthDiff
        height += heightDiff
      }
      Origin.BOTTOM_RIGHT -> {
        x -= widthDiff
        y -= heightDiff
        width += widthDiff
        height += heightDiff
      }
      Origin.CENTER -> {
        x -= widthDiff / 2
        y -= heightDiff / 2
        width += widthDiff
        height += heightDiff
      }
    }
  }

  fun bisect(section: Section): Rectangle2D? {
    when (section) {
      Section.TOP -> return Rectangle2D(x, y, width, height / 2)
      Section.BOTTOM -> return Rectangle2D(x, y + height / 2, width, height / 2)
      Section.LEFT -> return Rectangle2D(x, y, width / 2, height)
      Section.RIGHT -> return Rectangle2D(x + width / 2, y, width / 2, height)
    }
  }

  fun contains(point: SimplePoint2D): Boolean {
    return point.x <= max.x && point.x >= min.x && point.y <= max.y && point.y >= min.y
  }

  override fun render(g: GraphicsContext) {
    g.fill = fill
    g.stroke = stroke
    g.globalBlendMode = blendMode
    g.globalAlpha = opacity
    g.strokeRect(x, y, width, height)
  }

  enum class Section {
    TOP, BOTTOM, LEFT, RIGHT
  }

  enum class Origin {
    TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT, CENTER
  }

  companion object {

    fun convert(rect: Rectangle): Rectangle2D {
      return Rectangle2D(rect.x, rect.y, rect.width, rect.height)
    }

    fun convert(shape: Shape): Rectangle2D {
      val bounds = shape.boundsInLocal
      return Rectangle2D(bounds.minX, bounds.minY, bounds.width, bounds.height)
    }
  }
}
