package sttc.util.shape

import javafx.scene.canvas.GraphicsContext

/**

 */
interface Shape2D {
  fun render(g: GraphicsContext)
}
