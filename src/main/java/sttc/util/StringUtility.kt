package sttc.util

/**

 */
object StringUtility {
  @JvmStatic fun splitstrip(s: String, sub: String): Array<String?> {
    val strings: Array<String?>
    if (s.contains(sub)) {
      strings = s.split(sub.toRegex()).dropLastWhile(String::isEmpty).toTypedArray()
    } else {
      strings = arrayOfNulls<String>(1)
      strings[0] = s
    }
    for (i in strings.indices) {
      strings[i] = strings[i]?.replace("\\s+".toRegex(), "")
    }
    return strings
  }
}
