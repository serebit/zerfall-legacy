package sttc.util.input

import javafx.scene.Scene
import java.util.*

/**

 */
object InputMap {
  private val inputs = HashMap<Input, InputState>()

  init {
    for (input in Input.values()) {
      inputs.put(input, InputState.IDLE)
    }
  }

  fun init(scene: Scene) {
    scene.setOnKeyPressed { e -> InputMap.press(Input.getEquiv(e.code)) }
    scene.setOnKeyReleased { e -> InputMap.release(Input.getEquiv(e.code)) }
    scene.setOnMousePressed { e -> InputMap.press(Input.getEquiv(e.button)) }
    scene.setOnMouseReleased { e -> InputMap.release(Input.getEquiv(e.button)) }
  }

  fun getState(input: Input): InputState {
    val state = safeGet(input)
    when (state) {
      InputState.RELEASE -> {
        inputs.put(input, InputState.IDLE)
        return InputState.RELEASE
      }
      InputState.PRESS -> {
        inputs.put(input, InputState.ACTIVE)
        return InputState.PRESS
      }
      InputState.ACTIVE -> {
        return state
      }
      InputState.IDLE -> {
        return state
      }
    }
  }

  fun isPress(input: Input): Boolean {
    val state = getState(input)
    return state == InputState.PRESS
  }

  fun isActive(input: Input): Boolean {
    val state = getState(input)
    return state == InputState.PRESS || state == InputState.ACTIVE
  }

  fun isRelease(input: Input): Boolean {
    val state = getState(input)
    return state == InputState.RELEASE
  }

  fun isIdle(input: Input): Boolean {
    val state = getState(input)
    return state == InputState.RELEASE || state == InputState.IDLE
  }

  private fun press(input: Input) {
    if (!isActive(input)) {
      inputs.put(input, InputState.PRESS)
    }
  }

  private fun release(input: Input) {
    if (!isIdle(input)) {
      inputs.put(input, InputState.RELEASE)
    }
  }

  private fun safeGet(input: Input): InputState {
    if (!inputs.containsKey(input)) {
      throw IllegalArgumentException()
    } else {
      return inputs[input] as InputState
    }
  }
}
