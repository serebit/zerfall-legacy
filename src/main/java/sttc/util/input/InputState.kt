package sttc.util.input

/**

 */
enum class InputState {
  PRESS, ACTIVE, RELEASE, IDLE
}
