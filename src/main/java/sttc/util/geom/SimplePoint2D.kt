package sttc.util.geom

import javafx.geometry.Point2D

/**
 * Created by campbell on 1/2/17.
 */
class SimplePoint2D(val x: Double, val y: Double) {
  val toFx: Point2D by lazy {
    Point2D(x, y)
  }
}