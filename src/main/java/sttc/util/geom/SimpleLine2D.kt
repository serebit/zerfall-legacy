package sttc.util.geom

import sttc.util.Calculator
import sttc.util.shape.Line2D

/**
 * Created by campbell on 1/2/17.
 */
class SimpleLine2D(val start: SimplePoint2D, val end: SimplePoint2D) {
  val midpoint = SimplePoint2D(Calculator.avg(start.x, end.x), Calculator.avg(start.y, end.y))
  val width = Math.abs(end.x - start.x)
  val signedWidth = end.x - start.x
  val height = Math.abs(end.y - start.y)
  val signedHeight = end.y - start.y
  val length = Math.sqrt(Calculator.pwr(width, 2) + Calculator.pwr(height, 2))
  val toFx: Line2D by lazy {
    Line2D(start.toFx, end.toFx)
  }
}