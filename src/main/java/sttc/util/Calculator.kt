package sttc.util

import java.math.BigDecimal
import java.math.RoundingMode

/**

 */
object Calculator {
  @JvmStatic fun round(value: Double, places: Int): Double {
    if (places < 0) throw IllegalArgumentException()

    var bd = BigDecimal(value)
    bd = bd.setScale(places, RoundingMode.HALF_UP)
    return bd.toDouble()
  }

  @JvmStatic fun avg(vararg values: Int): Int {
    val sum = values.sum()
    return sum / values.size
  }

  @JvmStatic fun avg(vararg values: Float): Float {
    val sum = values.sum()
    return sum / values.size
  }

  @JvmStatic fun avg(vararg values: Double): Double {
    val sum = values.sum()
    return sum / values.size
  }

  @JvmStatic fun pwr(base: Int, exp: Int): Int {
    if (exp == 0) {
      return 1
    } else if (exp == 1) {
      return base
    } else {
      var result = 1
      for (exp in (1..exp)) {
        result *= base
      }
      return result
    }
  }

  @JvmStatic fun pwr(base: Float, exp: Int): Float {
    if (exp == 0) {
      return 1.toFloat()
    } else if (exp == 1) {
      return base
    } else {
      var result = 1.toFloat()
      for (exp in (1..exp)) {
        result *= base
      }
      return result
    }
  }

  @JvmStatic fun pwr(base: Double, exp: Int): Double {
    if (exp == 0) {
      return 1.0
    } else if (exp == 1) {
      return base
    } else {
      var result = 1.0
      for (exp in (1..exp)) {
        result *= base
      }
      return result
    }
  }

  @JvmStatic fun sqrt(base: Int): Int {
    if (base < 0) {
      throw IllegalArgumentException("Cannot find square root of negative number")
    }
    val EPSILON = 1E-15
    var t = base
    while (Math.abs(t - base / t) > EPSILON * t)
      t = Math.round((base / t + t) / 2.0).toInt()
    return t
  }
}
