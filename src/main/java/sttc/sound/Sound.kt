package sttc.sound

import java.io.IOException
import javax.sound.sampled.*

/**

 */
class Sound private constructor() {
  private val sourceClips = mutableMapOf<AudioSource, AudioClip>()
  private lateinit var info: DataLine.Info
  private lateinit var format: AudioFormat
  private lateinit var audio: ByteArray
  private var size = 0

  internal constructor(path: String) : this() {
    sounds.add(this)
    val stream = AudioLoader.getAudioStream(path)
    format = stream.format
    size = (format.frameSize * stream.frameLength).toInt()
    audio = ByteArray(size)
    info = DataLine.Info(Clip::class.java, format, size)
    try {
      stream.read(audio, 0, size)
    } catch (e: IOException) {
      e.printStackTrace()
    }
  }

  @Synchronized internal fun play(source: AudioSource) {
    if (!sourceClips.containsKey(source)) {
      val clip = AudioClip(info)
      try {
        clip.open(format, audio, 0, size)
        sourceClips[source] = clip
      } catch (e: LineUnavailableException) {
        e.printStackTrace()
      }
    }
    val clip = sourceClips[source]!!
    clip.start()
  }

  @Synchronized internal fun resume(source: AudioSource) {
    sourceClips[source]?.resume()
  }

  @Synchronized private fun resume() {
    sourceClips.values.forEach(AudioClip::resume)
  }

  @Synchronized internal fun pause(source: AudioSource) {
    sourceClips[source]?.pause()
  }

  @Synchronized private fun pause() {
    sourceClips.values.forEach(AudioClip::pause)
  }

  @Synchronized internal fun stop(source: AudioSource) {
    sourceClips[source]?.close()
  }

  @Synchronized private fun stop() {
    sourceClips.values.forEach(AudioClip::stop)
    sourceClips.clear()
  }

  internal fun setPan(source: AudioSource, pan: Double) {
    sourceClips[source]?.pan = pan
  }

  internal fun setGain(source: AudioSource, gain: Double) {
    sourceClips[source]?.gain = gain
  }

  internal fun isPlaying(source: AudioSource): Boolean {
    val clip = sourceClips[source]
    if (clip != null) {
      return clip.isPlaying()
    } else {
      return false
    }
  }

  companion object {
    private val sounds = mutableListOf<Sound>()

    fun resume() {
      sounds.forEach(Sound::resume)
    }

    fun pause() {
      sounds.forEach(Sound::pause)
    }

    fun stop() {
      sounds.forEach(Sound::stop)
    }

    fun add(sound: Sound) {
      sounds.add(sound)
    }
  }
}
