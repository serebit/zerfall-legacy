package sttc.sound

import javax.sound.sampled.*

/**
 * Created by gingerdeadshot on 1/16/17.
 */
class AudioClip(info: DataLine.Info) {
  private var clip = AudioSystem.getLine(info) as Clip
  var framePosition: Int = 0
    get() = clip.framePosition
  var longFramePosition: Long = 0
    get() = clip.longFramePosition
  var microsecondPosition: Long = 0
    get() = clip.microsecondPosition
  var frameLength = clip.frameLength
  var microsecondLength = clip.microsecondLength
  internal var gain = 0.0
    set(value) {
      field = value
      if (clip.isControlSupported(FloatControl.Type.PAN)) {
        val control = clip.getControl(FloatControl.Type.PAN) as FloatControl
        control.value = pan.toFloat()
      }
    }
  internal var pan = 0.0
    set(value) {
      field = value
      if (clip.isControlSupported(FloatControl.Type.MASTER_GAIN)) {
        val control = clip.getControl(FloatControl.Type.MASTER_GAIN) as FloatControl
        control.value = gain.toFloat()
      }
    }

  fun open(format: AudioFormat, data: ByteArray, offset: Int = 0, bufferSize: Int) {
    clip.open(format, data, offset, bufferSize)
  }

  fun start() {
    clip.stop()
    clip.framePosition = 0
    clip.start()
  }

  fun resume() {
    clip.start()
  }

  fun pause() {
    clip.stop()
  }

  fun stop() {
    clip.stop()
    clip.framePosition = 0
  }

  fun close() {
    clip.stop()
    clip.flush()
    clip.close()
  }

  fun isPlaying(): Boolean {
    return clip.framePosition <= clip.frameLength - 1
  }
}