package sttc.sound

/**
 * Created by campbell on 12/25/16.
 */
class AudioSource {
  fun play(sound: Sound) {
      sound.play(this)
  }

  fun resume(sound: Sound) {
    sound.resume(this)
  }

  fun pause(sound: Sound) {
    sound.pause(this)
  }

  fun stop(sound: Sound) {
    sound.stop(this)
  }

  fun setPan(sound: Sound, pan: Double) {
    sound.setPan(this, pan)
  }

  fun setGain(sound: Sound, gain: Double) {
    sound.setGain(this, gain)
  }

  fun isPlaying(sound: Sound): Boolean {
    return (sound.isPlaying(this))
  }
}