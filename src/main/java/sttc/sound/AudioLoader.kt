package sttc.sound

import javax.sound.sampled.AudioInputStream
import javax.sound.sampled.AudioSystem

/**
 *
 */
object AudioLoader {
  internal val cache = mutableMapOf<String, Sound>()

  fun getAudioStream(path: String): AudioInputStream {
    return AudioSystem.getAudioInputStream(AudioLoader::class.java.classLoader.getResource(path))
  }

  fun load(path: String): Sound {
    if (cache.containsKey(path)) {
      return cache[path]!!
    } else {
      val sound = Sound(path)
      cache.put(path, sound)
      return sound
    }
  }
}
