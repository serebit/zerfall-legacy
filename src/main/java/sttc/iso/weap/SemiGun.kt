package sttc.iso.weap

import javafx.scene.image.Image
import sttc.sound.AudioSource
import sttc.sound.Sound
import java.util.concurrent.TimeUnit

/**
 * Semi-automatic abstract weapon. Includes custom fire and reload logic.
 */
open class SemiGun(
    override val name: String,
    override val CLIP_SIZE: Int,
    override val CACHE_SIZE: Int,
    override val RELOAD_TIME: Int,
    override val DAMAGE: Int,
    override val WEIGHT: Double,
    override val fire: Sound,
    override val reload: Sound,
    override val outline: Image? = null
) : Gun() {


  override var state = State.IDLE
    get() {
      when (canCheck) {
        true -> return field
        false -> return State.IDLE
      }
    }
  override val audioSource = AudioSource()
  override var canCheck = true
  override var clip = CLIP_SIZE
  override var cache = CACHE_SIZE

  @Synchronized override fun pull() {
    if (state != State.RELOADING) {
      if (clip > 0) {
        state = State.FIRING
        audioSource.play(fire)
        clip -= 1
      } else
        audioSource.play(dry)
    }
  }

  override fun close() {
    // TODO figure out if this is necessary
  }

  override fun release() {
    // TODO figure out if this is necessary
  }

  override fun reload() {
    if (state != State.RELOADING) {
      if (clip < CLIP_SIZE && cache > 0) {
        clipOut()
        scheduler.schedule({ this.clipIn() }, RELOAD_TIME.toLong(), TimeUnit.MILLISECONDS)
      }
    }
  }

  private fun clipOut() {
    audioSource.play(reload)
    cache += clip
    clip = 0
    state = State.RELOADING
  }

  private fun clipIn() {
    if (cache >= CLIP_SIZE) {
      cache -= CLIP_SIZE
      clip = CLIP_SIZE
    } else {
      clip += cache
      cache = 0
    }
    state = State.IDLE
  }

  override fun engage() {
    canCheck = true
  }

  override fun cycle() {
    if (canCheck) {
      canCheck = false
      state = State.IDLE
    }
  }

  override fun copy(): Gun {
    return SemiGun(name, CLIP_SIZE, CACHE_SIZE, RELOAD_TIME, DAMAGE, WEIGHT, fire, reload, outline)
  }

  override fun sameModel(gun: Gun): Boolean {
    return (
        gun is SemiGun &&
            gun.CLIP_SIZE == CLIP_SIZE &&
            gun.CACHE_SIZE == CACHE_SIZE &&
            gun.name.equals(name) &&
            gun.RELOAD_TIME == RELOAD_TIME &&
            gun.DAMAGE == DAMAGE &&
            gun.WEIGHT == WEIGHT
        )
  }
}
