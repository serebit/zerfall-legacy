package sttc.iso.weap

import javafx.scene.image.Image
import sttc.sound.AudioLoader
import sttc.sound.AudioSource
import sttc.sound.Sound
import java.util.concurrent.ScheduledThreadPoolExecutor

/**

 */
abstract class Gun {
  @Transient internal val scheduler = ScheduledThreadPoolExecutor(1)
  abstract var state: State
  abstract var canCheck: Boolean
  abstract val audioSource: AudioSource
  abstract var clip: Int
  abstract var cache: Int
  abstract val CLIP_SIZE: Int
  abstract val CACHE_SIZE: Int
  abstract val RELOAD_TIME: Int
  abstract val DAMAGE: Int
  abstract val WEIGHT: Double
  abstract val name: String
  abstract val fire: Sound
  abstract val reload: Sound
  abstract val outline: Image?

  init {
    clip = CLIP_SIZE
    cache = CACHE_SIZE
  }

  abstract fun cycle()

  abstract fun engage()

  abstract fun close()

  abstract fun pull()

  abstract fun release()

  abstract fun reload()

  abstract fun copy(): Gun

  companion object {
    internal val dry = AudioLoader.load("guns/dry_fire.au")
  }

  enum class State {
    FIRING,
    CYCLING,
    IDLE,
    RELOADING
  }

  abstract fun sameModel(gun: Gun): Boolean
}
