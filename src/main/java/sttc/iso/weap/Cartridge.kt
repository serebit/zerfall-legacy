package sttc.iso.weap

/**
 * Created by campbell on 12/29/16.
 */
enum class Cartridge(
    val classification: String,
    val diameter: Double, // Bullet diameter in millimeters
    val weight: Double, // Bullet weight in grams
    val velocity: Double, // Bullet velocity in meters per second
    val damage: Int =
    ((0.5 * weight * Math.pow(velocity, 2.0)) * (Math.pow(diameter / 2, 2.0) * Math.PI) / 1000000).toInt()
) {

  WINCHESTER_300(".300 Winchester", 7.8, 11.66, 959.0),
  ACP_32(".32 ACP", 7.94, 4.21, 282.0),
  ACP_45(".45 ACP", 11.5, 11.99, 320.0),
  NATO_5_56("5.56x45mm NATO", 5.7, 4.08, 936.0),
  NATO_7_62("7.62x51mm NATO", 7.82, 9.53, 833.0),
  PARABELLUM_9("9x19mm Parabellum", 9.01, 7.45, 390.0),
  SOVIET_7_62("7.62x39mm", 7.92, 7.91, 730.3),
  TOKAREV_7_62("7.62x25mm Tokarev", 7.87, 5.51, 376.0)

}