package sttc.iso.weap

import javafx.scene.image.Image
import sttc.sound.AudioSource
import sttc.sound.Sound
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit


/**
 * Fully automatic abstract weapon. Includes custom fire and reload logic.
 */
open class AutoGun(
    override val name: String,
    override val CLIP_SIZE: Int,
    override val CACHE_SIZE: Int,
    val RATE_OF_FIRE: Int,
    override val RELOAD_TIME: Int,
    override val DAMAGE: Int,
    override val WEIGHT: Double,
    override val fire: Sound,
    override val reload: Sound,
    override val outline: Image? = null
) : Gun() {
  override var state = State.IDLE
    get() {
      when (canCheck) {
        true -> return field
        false -> return State.IDLE
      }
    }
  override val audioSource = AudioSource()
  override var canCheck = true
  private var cycle: ScheduledFuture<*>? = null
  override var clip = CLIP_SIZE
  override var cache = CACHE_SIZE

  override fun engage() {
    canCheck = true
  }

  override fun cycle() {
    if (canCheck) {
      canCheck = false
      state = State.IDLE
    }
  }

  override fun close() {
    release()
  }

  @Synchronized override fun pull() {
    if (state != State.RELOADING) {
      if (clip > 0) {
        state = State.FIRING
        audioSource.play(fire)
        clip--
        cycle = scheduler.scheduleAtFixedRate({
          if (clip == 0) {
            cycle?.cancel(true)
          } else {
            state = State.FIRING
            audioSource.play(fire)
            clip--
          }
        }, 6e10.toLong() / RATE_OF_FIRE, 6e10.toLong() / RATE_OF_FIRE, TimeUnit.NANOSECONDS)
      } else audioSource.play(Gun.dry)
    }
  }

  @Synchronized override fun release() {
    cycle?.cancel(false)
  }

  override fun reload() {
    if (state != State.RELOADING) {
      if (clip < CLIP_SIZE && cache > 0) {
        clipOut()
        scheduler.schedule({ this.clipIn() }, RELOAD_TIME.toLong(), TimeUnit.MILLISECONDS)
      }
    }
  }

  private fun clipOut() {
    audioSource.play(reload)
    cache += clip
    clip = 0
    state = State.RELOADING
  }

  private fun clipIn() {
    if (cache >= CLIP_SIZE) {
      cache -= CLIP_SIZE
      clip = CLIP_SIZE
    } else {
      clip += cache
      cache = 0
    }
    state = State.IDLE
  }

  override fun copy(): Gun {
    return AutoGun(name, CLIP_SIZE, CACHE_SIZE, RATE_OF_FIRE, RELOAD_TIME, DAMAGE, WEIGHT, fire, reload, outline)
  }

  override fun sameModel(gun: Gun): Boolean {
    return (
        gun is AutoGun &&
            gun.RATE_OF_FIRE == RATE_OF_FIRE &&
            gun.CLIP_SIZE == CLIP_SIZE &&
            gun.CACHE_SIZE == CACHE_SIZE &&
            gun.name.equals(name) &&
            gun.RELOAD_TIME == RELOAD_TIME &&
            gun.DAMAGE == DAMAGE &&
            gun.WEIGHT == WEIGHT
        )
  }
}
