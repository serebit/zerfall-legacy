package sttc.iso.anim

import com.fasterxml.jackson.databind.ObjectMapper
import javafx.scene.image.Image
import javafx.scene.image.WritableImage
import sttc.util.StringUtility
import java.io.InputStream
import java.util.*

/**

 */
class AnimationMap private constructor() {
  private var size: Int = 0
  private var values: MutableList<Animation>

  init {
    values = ArrayList<Animation>()
  }

  constructor(dataStream: InputStream, sheetStream: InputStream) : this() {
    val sheet = Image(sheetStream)
    val data = ObjectMapper().readTree(dataStream)
    val animationData = data.get("animations")
    val width = data.get("width").asInt()
    val height = data.get("height").asInt()
    for (i in 0..animationData.size() - 1) {
      val animData = animationData.get(i)
      val actions = StringUtility.splitstrip(animData.get("action").asText(), ",")
      val states = StringUtility.splitstrip(animData.get("state").asText(), ",")
      val directions = StringUtility.splitstrip(animData.get("direction").asText(), ",")
      val hash = animData.get("hash").asInt()
      val numFrames = animData.get("frames").asInt()
      val x = i * width

      val frameList = (0..numFrames - 1)
          .map { it * height }
          .map { WritableImage(sheet.pixelReader, x, it, width, height) }
      for (action in actions)
        for (state in states)
          for (direction in directions)
            put(Animation(action!!, state!!, direction!!, hash, frameList))
    }
    size = values.size
  }

  operator fun get(action: String, state: String, direction: String): Animation? {
    return (0..size - 1)
        .first { checkEq(values[it], action, state, direction) }.let { values[it] }
  }

  private fun put(anim: Animation) {
    var insert = true
    for (i in 0..size - 1) {
      if (checkEq(values[i], anim)) {
        values[i] = anim
        insert = false
      }
    }
    if (insert) {
      values.add(anim)
    }
  }

  private fun checkEq(entry: Animation, given: Animation): Boolean {
    return entry == given
  }

  private fun checkEq(entry: Animation?, act: String, stat: String, dir: String): Boolean {
    val animAction = entry?.action
    val animState = entry?.state
    val animDirection = entry?.direction
    return animAction == act && animState == stat && animDirection == dir
  }

  fun remove(act: String, stat: String, dir: String) {
    for (i in 0..size - 1) {
      if (checkEq(values[i], act, stat, dir)) {
        values.remove(values[i])
        size--
        condenseArray(i)
      }
    }
  }

  private fun condenseArray(start: Int) {
    System.arraycopy(values, start + 1, values, start, size - start)
  }
}
