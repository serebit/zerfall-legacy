package sttc.iso.anim

import javafx.scene.image.WritableImage
import java.util.*

/**

 */
class Animation private constructor() {
  private var frames: List<WritableImage>
  private var index: Int = 0
  var action: String = ""
  var state: String = ""
  var direction: String = ""
  internal var hash: Int = 0

  init {
    this.frames = ArrayList<WritableImage>()
    index = 0
  }

  private constructor(frames: List<WritableImage>) : this() {
    this.frames = frames
    index = 0
  }

  internal constructor(action: String, state: String, direction: String, hash: Int, frames: List<WritableImage>) : this(frames) {
    this.action = action
    this.state = state
    this.direction = direction
    this.hash = hash
  }

  internal val attributes: Map<String, Any>
    get() {
      val attributes = HashMap<String, Any>()
      attributes.put("action", action)
      attributes.put("state", state)
      attributes.put("direction", direction)
      attributes.put("hash", hash)
      return attributes
    }

  val frame: WritableImage
    get() {
      val frame = frames[index]
      nextFrame()
      return frame
    }

  private fun nextFrame() {
    index++
    if (index == frames.size) {
      reset()
    }
  }

  private fun reset() {
    index = 0
  }
}