package sttc.iso.effect

import javafx.scene.canvas.GraphicsContext
import sttc.iso.world.Layer
import sttc.util.shape.Shape2D
import java.util.*

/**

 */
class Effect(val layer: Layer, vararg frames: Shape2D) {
  private var index: Int = 0
  private var frames: MutableList<Shape2D>

  init {
    index = 0
    this.frames = Arrays.asList(*frames)
  }

  fun render(g: GraphicsContext) {
    frames[index].render(g)
    index++
  }

  fun finished(): Boolean {
    return index >= frames.size
  }
}
