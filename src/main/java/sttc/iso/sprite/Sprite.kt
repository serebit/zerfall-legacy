package sttc.iso.sprite

import com.fasterxml.jackson.databind.ObjectMapper
import javafx.scene.image.WritableImage
import sttc.iso.anim.Animation
import sttc.iso.anim.AnimationMap

class Sprite(folder: String) {
  var width = 0
  var height = 0
  @Transient val animations: AnimationMap
  @Transient private var activeAnim: Animation? = null

  init {
    val dataPath = folder + "/metadata.json"
    val sheetPath = folder + "/sheet.png"

    val dataStream = Sprite::class.java.classLoader.getResourceAsStream(dataPath)
    val sheetStream = Sprite::class.java.classLoader.getResourceAsStream(sheetPath)

    val data = ObjectMapper().readTree(Sprite::class.java.classLoader.getResourceAsStream(dataPath))!!
    width = data.get("width").asInt()
    height = data.get("height").asInt()
    animations = AnimationMap(dataStream, sheetStream)
  }

  fun playAnim(action: String, state: String, direction: String) {
    activeAnim = animations[action, state, direction]
  }

  var action: String
    get() = activeAnim!!.action
    set(value) {
      activeAnim = animations[value, state, direction]
    }

  var state: String
    get() = activeAnim!!.state
    set(value) {
      activeAnim = animations[action, value, direction]
    }

  var direction: String
    get() = activeAnim!!.direction
    set(value) {
      activeAnim = animations[action, state, value]
    }

  fun get(): WritableImage {
    return activeAnim!!.frame
  }
}