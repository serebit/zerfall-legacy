package sttc.iso.input

enum class Action {
  MOVE_LEFT, MOVE_RIGHT, HOLD_CROUCH, TOGGLE_CROUCH, JUMP, SPRINT, SHOOT, RELOAD, MELEE, USE
}