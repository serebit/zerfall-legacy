package sttc.iso.input

import sttc.util.input.Input
import sttc.util.input.InputMap

object Controller {
  private lateinit var activeScheme: ControlScheme
  private lateinit var controls: Map<Action, Input>

  fun getInput(action: Action): Input {
    return safeGet(action)
  }

  fun getState(action: Action): ActionState? {
    safeGet(action)
    return ActionState.convert(InputMap.getState(safeGet(action)))
  }

  fun isStart(action: Action): Boolean {
    return InputMap.isPress(safeGet(action))
  }

  fun isActive(action: Action): Boolean {
    return InputMap.isActive(safeGet(action))
  }

  fun isEnd(action: Action): Boolean {
    return InputMap.isRelease(safeGet(action))
  }

  fun isInactive(action: Action): Boolean {
    return InputMap.isIdle(safeGet(action))
  }

  @JvmStatic fun setControlScheme(scheme: ControlScheme) {
    activeScheme = scheme
    controls = scheme.getControls()
  }

  @JvmStatic val schemeName: String
    get() = activeScheme.name

  fun safeGet(action: Action): Input {
    if (!controls.containsKey(action)) {
      throw IllegalArgumentException()
    } else {
      return controls[action] as Input
    }
  }
}