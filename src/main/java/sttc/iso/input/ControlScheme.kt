package sttc.iso.input

import sttc.util.input.Input
import java.util.*

/**

 */
open class ControlScheme(val name: String) {
  private val controls: MutableMap<Action, Input>

  init {
    this.controls = HashMap<Action, Input>()
  }

  protected fun map(action: Action, input: Input) {
    controls.put(action, input)
  }

  fun getControls(): Map<Action, Input> {
    return HashMap(controls)
  }
}
