package sttc.iso.input

import sttc.util.input.InputState

import java.util.Arrays

/**

 */
enum class ActionState constructor(private val equivalent: InputState) {
  START(InputState.PRESS), ACTIVE(InputState.ACTIVE), END(InputState.RELEASE), INACTIVE(InputState.IDLE);


  companion object {

    fun convert(inputState: InputState): ActionState {
      return Arrays.stream(ActionState.values()).filter { s -> s.equivalent == inputState }.findAny().orElse(null)
    }
  }
}
