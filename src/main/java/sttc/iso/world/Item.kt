package sttc.iso.world

import javafx.scene.canvas.GraphicsContext
import javafx.scene.image.Image

open class Item(layer: Layer, x: Double, y: Double, w: Double, h: Double, var texture: Image? = null)
  : Thing(x, y, w, h, layer) {

  override fun render(g: GraphicsContext) {
    g.save()
    if (texture == null) {
      g.fill = layer.defaultFill
      g.stroke = layer.defaultStroke
      g.fillRect(x, y, width, height)
    } else {
      g.canvas.clip = bounds
      g.drawImage(texture, x, y, width, height)
      g.canvas.clip = null
    }
    g.restore()
  }
}
