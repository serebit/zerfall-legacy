package sttc.iso.world

import javafx.geometry.Point2D
import javafx.scene.canvas.Canvas
import javafx.scene.canvas.GraphicsContext
import javafx.scene.image.Image
import javafx.scene.layout.StackPane
import javafx.scene.paint.Paint
import javafx.scene.shape.Shape
import sttc.iso.effect.Effect
import sttc.util.geom.SimpleLine2D
import sttc.util.geom.SimplePoint2D
import sttc.util.shape.Rectangle2D
import java.util.*

/**
 * The game world. Contains items (such as floors, platforms and walls) and
 * npcs (such as the player).
 *
 *
 * Due to the nature of JavaFX, the GraphicsContext is not stored in the save
 * file. The main class must pass the currently loaded Canvas to the loaded Environment.
 */
abstract class Environment(val gravity: Double) {
  protected abstract var items: MutableList<Item>
  protected abstract var npcs: MutableList<Npc>
  protected abstract var effectQueue: MutableList<Effect>
  protected abstract var player: Player
  protected abstract var dialog: String
  protected abstract var skybox: Paint

  abstract fun execute()

  protected fun spawn(npc: Npc) {
    npcs.add(npc)
    npc.gravity = gravity
  }

  protected fun place(vararg items: Item) {
    this.items.addAll(Arrays.asList(*items))
  }

  abstract fun render(surface: GraphicsContext)

  protected fun spawnPlayer(p: Player) {
    this.player = p
    p.gravity = gravity
  }

  protected fun <R> filterItems(klass: Class<R>): List<R> {
    val list = items.filterIsInstance(klass)
    return list
  }

  protected fun filterItems(layer: Layer): List<Item> {
    val list = items.filter { it.layer == layer }
    return list
  }

  protected fun filterEffects(layer: Layer): List<Effect> {
    val list = effectQueue.filter { it.layer == layer }
    return list
  }

  protected fun intersectItems(line: SimpleLine2D, layer: Layer): List<Item> {
    val list = filterItems(layer).filterNot { Rectangle2D.convert(Shape.intersect(line.toFx, it.bounds)).isEmpty }
    return list
  }

  protected fun intersectNpcs(line: SimpleLine2D): List<Npc> {
    val list = npcs.toMutableSet().filterNot { Rectangle2D.convert(Shape.intersect(line.toFx, it.bounds)).isEmpty }
    return list
  }

  protected fun castRay(line: SimpleLine2D, layer: Layer, source: Thing): Thing? {
    val things = mutableListOf<Thing>()
    things.addAll(intersectItems(line, layer))
    things.addAll(intersectNpcs(line))
    things.add(player)
    things.remove(source)
    var scan = Point2D(line.start.x, line.start.y)
    val normalEnd = Point2D(Math.abs(line.end.x - line.start.x), Math.abs(line.end.y - line.start.y))
    while (Math.abs(scan.x - line.start.x) <= normalEnd.x && Math.abs(scan.y - line.start.y) <= normalEnd.y) {
      val thing = things.firstOrNull { it.bounds.contains(scan) }
      if (thing != null) return thing
      scan = Point2D(scan.x + line.signedWidth / line.length, scan.y + line.signedHeight / line.length)
    }
    return null
  }

  protected fun getCastRay(line: SimpleLine2D, layer: Layer): SimpleLine2D {
    val things = mutableListOf<Thing>()
    things.addAll(intersectItems(line, layer))
    things.addAll(intersectNpcs(line))
    var scan = line.start
    val normalEnd = SimplePoint2D(Math.abs(line.end.x - line.start.x), Math.abs(line.end.y - line.start.y))
    while (Math.abs(scan.x - line.start.x) <= normalEnd.x && Math.abs(scan.y - line.start.y) <= normalEnd.y) {
      val thing = things.firstOrNull { it.bounds.contains(scan) }
      if (thing != null) return SimpleLine2D(line.start, scan)
      scan = SimplePoint2D(scan.x + line.signedWidth / line.length, scan.y + line.signedHeight / line.length)
    }
    return line
  }

  protected fun purgeEffects() {
    val list = effectQueue.filter(Effect::finished)
    effectQueue.removeAll(list)
  }

  fun readyForReset(): Boolean {
    return player.health <= 0
  }

  fun close() {
    items.clear()
    npcs.clear()
    effectQueue.clear()
    dialog = ""
  }
}
