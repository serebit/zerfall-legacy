package sttc.iso.world

import javafx.scene.image.Image

/**

 */
class Spawner(layer: Layer, x: Double, y: Double, width: Double, height: Double, texture: Image, private var entity: Entity, private var isActive: Boolean) : Item(layer, x, y, width, height) {

  init {
    this.texture = texture
  }

  fun activate() {
    isActive = true
  }

  fun deactivate() {
    isActive = false
  }

  fun setEntity(entity: Entity) {
    entity.setLocation(x, y)
    this.entity = entity
  }

  val entityClass: Class<out Entity>
    get() = entity.javaClass

  fun spawn(): Entity? {
    if (isActive) {
      entity.copy().setLocation(x, y)
      return entity.copy()
    } else {
      return null
    }
  }
}
