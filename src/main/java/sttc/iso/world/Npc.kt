package sttc.iso.world

/**

 */
abstract class Npc : Entity {
  var target: Entity? = null

  constructor(x: Double, y: Double, width: Double, height: Double, health: Double) : super(x, y, width, height, health)

  constructor(x: Double, y: Double, health: Double) : super(x, y, health)

  companion object {
    lateinit var player: Player
  }
}
