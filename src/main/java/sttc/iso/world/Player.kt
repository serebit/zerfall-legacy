package sttc.iso.world

import javafx.scene.shape.Line
import sttc.iso.weap.Gun
import sttc.util.geom.SimpleLine2D

/**

 */
abstract class Player : Entity {
  constructor(x: Double, y: Double, width: Double, height: Double, health: Double) : super(x, y, width, height, health)

  constructor(x: Double, y: Double, health: Double) : super(x, y, health)

  abstract var gun: Gun

  abstract fun isShooting(): Boolean

  abstract fun getBulletTrajectory(): SimpleLine2D

  abstract fun withdrawPoints(amount: Int): Boolean

  abstract fun depositPoints(amount: Int)
}
