package sttc.iso.world

import java.util.concurrent.Future
import java.util.concurrent.ScheduledThreadPoolExecutor
import java.util.concurrent.TimeUnit

/**
 * An item that can be interacted with.
 */
class CollectableItem(interact: Runnable, dialog: String, distance: Layer,
                      x: Double, y: Double, w: Double, h: Double) : Item(distance, x, y, w, h) {
  private var action: Runnable? = null
  private var task: Future<*>? = null
  private val scheduler = ScheduledThreadPoolExecutor(1)
  var dialog = ""
  var collected: Boolean = false

  init {
    this.setAction(interact)
    this.dialog = dialog
  }

  fun setAction(interact: Runnable) {
    this.action = interact
  }

  fun collect() {
    if (task == null || task!!.isDone) {
      task = scheduler.submit(action)
    }
    scheduler.shutdown()
    try {
      scheduler.awaitTermination(10, TimeUnit.MINUTES)
    } catch (e: InterruptedException) {
      e.printStackTrace()
    }

  }
}
