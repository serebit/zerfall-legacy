package sttc.iso.world

import javafx.scene.canvas.GraphicsContext
import javafx.scene.shape.Shape

/**
 * Abstract class based off of the Sprite class. For in-game entities that have
 * movement capability and collect with physics. The main class must have its
 * own implementation of this subclass.
 */
abstract class Entity : Thing, Cloneable {
  protected var prevX: Double = 0.toDouble()
  protected var prevY: Double = 0.toDouble()
  protected var yVelocity: Double = 0.toDouble()
  protected var xVelocity: Double = 0.toDouble()
  var gravity: Double = 0.toDouble()
  var timeScale: Double = 0.toDouble()
  var health: Double = 0.toDouble()
    protected set
  protected var maxHealth: Double = 0.toDouble()

  constructor(x: Double, y: Double, width: Double, height: Double, health: Double) : super(x, y, width, height, Layer.LAYER2) {
    this.health = health
    this.maxHealth = health
  }

  constructor(x: Double, y: Double, health: Double) : super(x, y, Layer.LAYER2) {
    this.health = health
    this.maxHealth = health
  }

  abstract fun collide(shape: Shape)

  fun setLocation(x: Double, y: Double) {
    this.x = x
    this.y = y
  }

  protected fun storeLocation() {
    prevX = x
    prevY = y
  }

  fun execute() {
    storeLocation()
    movement()
    physics()
    mechanics()
  }

  protected abstract fun movement()

  protected abstract fun mechanics()

  protected abstract fun physics()

  abstract fun visual()

  abstract fun start()

  abstract fun finish()

  open fun damage(amount: Double) {
    if (health > amount) {
      health -= amount
    } else {
      health = 0.0
    }
  }

  fun heal(amount: Double) {
    if (health + amount <= maxHealth) {
      health += amount
    } else {
      health = maxHealth
    }
  }

  abstract override fun render(g: GraphicsContext)

  abstract fun copy(): Entity
}