package sttc.iso.world

import javafx.scene.paint.Color

/**

 */
enum class Layer {
  LAYER4, LAYER3, LAYER2, LAYER1, LAYER0;

  var defaultStroke: Color? = null
    private set
  var defaultFill: Color? = null
    private set
  var layer: Int = 0

  companion object {

    init {
      Layer.LAYER0.defaultFill = Color.rgb(250, 250, 250)
      Layer.LAYER0.defaultStroke = Color.rgb(245, 245, 245)
      Layer.LAYER0.layer = 0
      Layer.LAYER1.defaultFill = Color.rgb(238, 238, 238)
      Layer.LAYER1.defaultStroke = Color.rgb(224, 224, 224)
      Layer.LAYER1.layer = 1
      Layer.LAYER2.defaultFill = Color.rgb(189, 189, 189)
      Layer.LAYER2.defaultStroke = Color.rgb(158, 158, 158)
      Layer.LAYER2.layer = 2
      Layer.LAYER3.defaultFill = Color.rgb(117, 117, 117)
      Layer.LAYER3.defaultStroke = Color.rgb(97, 97, 97)
      Layer.LAYER3.layer = 3
      Layer.LAYER4.defaultFill = Color.rgb(66, 66, 66)
      Layer.LAYER4.defaultStroke = Color.rgb(33, 33, 33)
      Layer.LAYER4.layer = 4
    }
  }
}
