package sttc.iso.world

import javafx.scene.canvas.GraphicsContext
import sttc.util.shape.Rectangle2D

/**

 */
abstract class Thing {
  var x = 0.0
  var y = 0.0
  var width = 0.0
  var height= 0.0
  var layer: Layer

  constructor(x: Double, y: Double, width: Double, height: Double, layer: Layer) {
    this.x = x
    this.y = y
    this.width = width
    this.height = height
    this.layer = layer
  }

  constructor(x: Double, y: Double, layer: Layer) {
    this.x = x
    this.y = y
    this.layer = layer
  }

  val bounds: Rectangle2D
    get() = Rectangle2D(x, y, width, height)

  abstract fun render(g: GraphicsContext)
}
