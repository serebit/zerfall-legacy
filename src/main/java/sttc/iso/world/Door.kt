package sttc.iso.world

import com.sun.istack.internal.NotNull
import sttc.sound.AudioSource
import sttc.sound.Sound
import javafx.scene.image.Image
import java.util.*

/**
 * It's a door.
 */
class Door(x: Double, y: Double, width: Double, height: Double, texture: Image?, dialog: String?, cost: Int,
           @NotNull private val openSound: Sound, vararg links: Spawner)
  : InteractiveItem(x, y, width, height, Layer.LAYER2) {
  private val audioSource = AudioSource()
  private val links: List<Spawner>
  private var cost = 0
  private var isOpen = false

  init {
    this.dialog = dialog + " (Cost: " + cost.toString() + ")"
    this.texture = texture
    this.links = Arrays.asList(*links)
    this.cost = cost
    isOpen = false
  }

  override fun runAction() {
    if (!isOpen) {
      if (player.withdrawPoints(cost)) {
        layer = Layer.LAYER3
        links.forEach(Spawner::activate)
        isOpen = true
        dialog = ""
        audioSource.play(openSound)
      }
    }
  }

  override fun toString(): String {
    return "Door [x = " +
        x +
        ", y = " +
        y +
        ", width = " +
        width +
        ", height = " +
        height +
        ", isOpen = " +
        isOpen +
        "]"
  }

  companion object {
    lateinit var player: Player
  }
}
