package sttc.iso.world

import com.sun.istack.internal.NotNull
import javafx.scene.image.Image
import java.util.concurrent.Future
import java.util.concurrent.ScheduledThreadPoolExecutor

/**
 * An item that can be interacted with.
 */
open class InteractiveItem : Item {
  private val scheduler = ScheduledThreadPoolExecutor(1)
  private var action: Runnable? = null
  private var task: Future<*>? = null
  open var dialog = ""

  protected constructor(x: Double, y: Double, width: Double, height: Double, layer: Layer)
      : super(layer, x, y, width, height)

  protected constructor(x: Double, y: Double, width: Double, height: Double, layer: Layer, @NotNull texture: Image)
      : super(layer, x, y, width, height, texture)

  open fun runAction() {
    if (task == null || task!!.isDone) {
      task = scheduler.submit(action)
    }
  }
}
