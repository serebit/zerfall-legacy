import javafx.geometry.Point2D
import org.junit.Assert
import org.junit.Test
import sttc.util.Calculator
import sttc.util.shape.Line2D

/**
 * Created by campbell on 12/31/16.
 */

class Tests {
  @Test fun testCalculator() {
    Assert.assertEquals(16, Calculator.pwr(2, 4))
    Assert.assertEquals(3, Calculator.sqrt(9))
  }

  @Test fun testLine2D() {
    Assert.assertEquals(
        mutableListOf(
            Line2D(Point2D(10000.0, 10000.0), Point2D(10001.0, 10001.0)),
            Line2D(Point2D(10001.0, 10001.0), Point2D(10002.0, 10002.0))
        ).toString(),
        Line2D(Point2D(10000.0, 10000.0), Point2D(10002.0, 10002.0)).split(2).toString()
    )
    Assert.assertEquals(
        mutableListOf(
            Line2D(Point2D(0.0, 0.0), Point2D(1.0, 1.0)),
            Line2D(Point2D(1.0, 1.0), Point2D(2.0, 2.0))
        ).toString(),
        Line2D(Point2D(0.0, 0.0), Point2D(2.0, 2.0)).split(2).toString()
    )
  }
}